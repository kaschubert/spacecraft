Shader "Ship" {
Properties {
	_MainColor ("Main Color", Color) = (1,1,1,1)
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_TeamColor ("Team Color", Color) = (1,1,1,1)
	_TeamTex ("Teamcolor (RGBA)", 2D) = "black" {}
	_BadgeTex ("Badge (RGBA)", 2D) = "black" {}
}

SubShader {
	Tags { "RenderType"="Opaque" }
	LOD 250
	
CGPROGRAM
#pragma surface surf Lambert

sampler2D _MainTex;
sampler2D _TeamTex;
sampler2D _BadgeTex;
float4 _MainColor;
float4 _TeamColor;

struct Input {
	float2 uv_MainTex;
	float2 uv_TeamTex;
	float2 uv_BadgeTex;
};

void surf (Input IN, inout SurfaceOutput o) {
	float4 c = tex2D(_MainTex, IN.uv_MainTex);
	half4 team = tex2D(_TeamTex, IN.uv_TeamTex);
	half4 badge = tex2D(_BadgeTex, IN.uv_BadgeTex);
	half4 d;
	team *= _TeamColor;
	d.rgb = lerp (team.rgb, badge.rgb, badge.a);
	c.rgb = lerp (c.rgb, d.rgb, d.a);
	c *= _MainColor;
	o.Albedo = c.rgb;
	o.Alpha = c.a;
}
ENDCG
}

Fallback "Diffuse"
}
