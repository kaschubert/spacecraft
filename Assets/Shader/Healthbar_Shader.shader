Shader "Healthbar_Shader" {
	Properties {
		_lifeColor ("Life Color", Color) = (0,1,0,1)
		_deathColor ("Death Color", Color) = (1,0,0,1)
		_barTex ("Bar Texture", 2D) = "white" {}

	}
	SubShader {
		Pass{
			Lighting On

			SetTexture [_barTex] {
                combine texture
            }
		}

	} 
	FallBack "Diffuse"
}
