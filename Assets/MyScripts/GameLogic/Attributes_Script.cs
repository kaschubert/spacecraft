using UnityEngine;
using System.Collections;

public class Attributes_Script : MonoBehaviour {
	public int      _costs = 200;
	public int      _player = 1;
	public float    _maxHealth = 400.0f;
	public float    _currentHealth = 400.0f;
    public int      _maxTargetCount = 3;
	public float    _attackRadius = 100;
	public float	_maxMovementDistance = 75;
	public float	_explosionRadius = 50;
	public float	_speed = 1;
	public Vector3	_instantiationCoord;
	public bool 	_warpPointSet = false;

	void Start ()
    {

	}

	void Update ()
    {

	}
}
