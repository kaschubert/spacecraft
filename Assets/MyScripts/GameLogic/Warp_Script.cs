using UnityEngine;
using System.Collections;

public class Warp_Script : BBTouchableObject {
	public Vector3 _warpTo;
	public Quaternion _warpRot;
	ShipStateManager_Script _shipState_Script;
	
	
	// Use this for initialization
	void Start () {		
		//Scripts used
		_shipState_Script = GetComponent<ShipStateManager_Script>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void warp(){
		//Minimal. Insert animation here.
		//TODO Bewegungsskript daran hindern, das schiff TROTZDEM an die geklickte Position zu schicken
		transform.position = _warpTo;
		transform.rotation = _warpRot;
	}
	
	public void setWarpPoint(Vector3 warpTo, Quaternion rotation){
		_warpTo = warpTo;
		_warpRot = rotation;
	}
	
	public override void handleSingleTouch(BBTouchEvent touch){
		if(_shipState_Script._currentState == ShipStateManager_Script.FIGHT_MOVED){
			warp();
			_shipState_Script._currentState = ShipStateManager_Script.FIGHT_ENABLED;
		}
	}
}
