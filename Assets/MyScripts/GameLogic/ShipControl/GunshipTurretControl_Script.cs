using UnityEngine;
using System.Collections;

public class GunshipTurretControl_Script : TurretControl_Script {

	protected override void Start4Derived()
	{
		_shotsPerSalve = 6;
		_shotPrototype = GameObject.Find("Gunship_Shot");
        Transform guns = transform.FindChild("Turret_AA/Turret_Base/Guns_Ani_U_D/Turret_Guns");

        _muzzles.Add(guns.Find("Muzzle_L_1").gameObject);
        _muzzles.Add(guns.Find("Muzzle_L_2").gameObject);
        _muzzles.Add(guns.Find("Muzzle_L_3").gameObject);

        _muzzles.Add(guns.Find("Muzzle_R_1").gameObject);
        _muzzles.Add(guns.Find("Muzzle_R_2").gameObject);
        _muzzles.Add(guns.Find("Muzzle_R_3").gameObject);
	}
}
