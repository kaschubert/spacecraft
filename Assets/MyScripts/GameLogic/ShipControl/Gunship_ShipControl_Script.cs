using UnityEngine;
using System.Collections;

public class Gunship_ShipControl_Script : ShipControl_Script {
	
	private string[] _turretPaths;
	private string[] _gunPaths;
	private Material[] _turretMats;
	private Material[] _gunMats;
	
	protected override void Start4Derived()
	{		
		_shipMat = transform.FindChild("Gunship_lowPoly_model").renderer.material;
		_shipMat.SetColor("_TeamColor", _teamColor);
		_shipMat.SetColor("_MainColor", _activeColor);
		
		_turretPaths = new string[5];
		_turretPaths[0] = "Gunship_lowPoly_model/Weapon_Turretport_Bottom_Front/Turret_AA/Turret_Base";
		_turretPaths[1] = "Gunship_lowPoly_model/Weapon_Turretport_Bottom_Rear/Turret_AA/Turret_Base";
		_turretPaths[2] = "Gunship_lowPoly_model/Weapon_Turretport_Top_Front_1/Turret_AA/Turret_Base";
		_turretPaths[3] = "Gunship_lowPoly_model/Weapon_Turretport_Top_Front_2/Turret_AA/Turret_Base";
		_turretPaths[4] = "Gunship_lowPoly_model/Weapon_Turretport_Top_Rear/Turret_AA/Turret_Base";
		
		_turretMats = new Material[5];
		for(int i = 0 ; i < _turretMats.Length ; i++){
			_turretMats[i] = transform.FindChild(_turretPaths[i]).renderer.material;
		}
		
		_gunPaths = new string[5];
		_gunPaths[0] = "Gunship_lowPoly_model/Weapon_Turretport_Bottom_Front/Turret_AA/Turret_Base/Guns_Ani_U_D/Turret_Guns";
		_gunPaths[1] = "Gunship_lowPoly_model/Weapon_Turretport_Bottom_Rear/Turret_AA/Turret_Base/Guns_Ani_U_D/Turret_Guns";
		_gunPaths[2] = "Gunship_lowPoly_model/Weapon_Turretport_Top_Front_1/Turret_AA/Turret_Base/Guns_Ani_U_D/Turret_Guns";
		_gunPaths[3] = "Gunship_lowPoly_model/Weapon_Turretport_Top_Front_2/Turret_AA/Turret_Base/Guns_Ani_U_D/Turret_Guns";
		_gunPaths[4] = "Gunship_lowPoly_model/Weapon_Turretport_Top_Rear/Turret_AA/Turret_Base/Guns_Ani_U_D/Turret_Guns";
		
		_gunMats = new Material[5];
		for(int i = 0 ; i < _gunMats.Length ; i++){
			_gunMats[i] = transform.FindChild(_gunPaths[i]).renderer.material;
		}
		
		
		string path2Engine = "Gunship_lowPoly_model/Engine_Main_Center/Engine_Blast";
		
		_engineBlast.Add(transform.Find(path2Engine).gameObject);
	}	
	
	public override void switchStatusColor(bool active){
		if(active){
			_shipMat.SetColor("_MainColor", _activeColor);
			
			for(int i = 0 ; i < _turretMats.Length ; i++){
				_turretMats[i].SetColor("_MainColor", _activeColor);
			}
			
			for(int i = 0 ; i < _gunMats.Length ; i++){
				_gunMats[i].SetColor("_MainColor", _activeColor);
			}
			
		} else {
			_shipMat.SetColor("_MainColor", _inactiveColor);
			
			for(int i = 0 ; i < _turretMats.Length ; i++){
				_turretMats[i].SetColor("_MainColor", _inactiveColor);
			}
			
			for(int i = 0 ; i < _gunMats.Length ; i++){
				_gunMats[i].SetColor("_MainColor", _inactiveColor);
			}
		}
	}
	
}
