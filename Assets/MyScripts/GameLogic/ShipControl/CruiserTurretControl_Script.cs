using UnityEngine;
using System.Collections;

public class CruiserTurretControl_Script : TurretControl_Script {

	protected override void Start4Derived()
	{
		_shotsPerSalve = 2;
		_shotPrototype = GameObject.Find("Cruiser_Shot");
		
		string muzzle = "/Turret_Base/Gun_Ani_U_D/Gun/Muzzle";
		
		GameObject test1 = transform.Find("Turret_CruiserTop" + muzzle).gameObject;
		_muzzles.Add(test1);
		
		GameObject test2 = transform.Find("Turret_CruiserBottom" + muzzle).gameObject;
        _muzzles.Add(test2);
	}
}
