using UnityEngine;
using System.Collections;

public class TurretControl_Script : MonoBehaviour
{
    private Quaternion      _turretDestinationRotation;
    public bool             _isTurning;
    public bool             _ready2fire;
    protected GameObject      _shotPrototype;
    protected ArrayList       _shots;
    protected ArrayList       _muzzles;
    private int             _muzzleCounter = 0;
    private float           _timeGone;
    protected GameObject      _target;
	protected int 			_muzzleCount;
	protected int 			_shotsPerSalve;
	public bool				_isAttacking = false;
	private bool 			_isFiring = false;
	private bool			_isWaitingForShotsToExpire = false;
	public bool				_hasShot = false;
	public int 				_shotsTravelling = 0;
	public int				_shotsTotal = 0;
	
	protected virtual void Start4Derived()
	{
	}
	
	void Start ()
    {
        _shots = new ArrayList();
        _muzzles = new ArrayList();
		
		Start4Derived();
		
		_muzzleCount = _muzzles.Count;
		//print("Muzzle Count: " + _muzzleCount);
	}
	
	void FixedUpdate () 
    {
//	    if(_isTurning)
//        {
//            transform.rotation = Quaternion.Lerp(transform.rotation, _turretDestinationRotation, Time.deltaTime);
//
//            //ready to fire
//            if (Quaternion.Angle(_turretDestinationRotation, transform.rotation) <= 1.0f)
//            {
//                _ready2fire = true;
//            }
//        }
//        else if(! _isTurning)
//        {
//            transform.rotation = Quaternion.Lerp(transform.rotation, transform.parent.transform.rotation, Time.deltaTime);
//        }
//
//        _timeGone += Time.deltaTime;
//
//        if (_ready2fire && _timeGone > 0.5f)
//        {
//            _timeGone = 0.0f;
//			
//			//print("_muzzleCounter: " + _muzzleCounter);
//			//print("_muzzleCount: " + _muzzleCount);
//			
//            GameObject muzzle = _muzzles[_muzzleCounter] as GameObject;
//			
//			//Go modulo through muzzles
//            if (_muzzleCounter < _muzzleCount-1)
//            {
//                _muzzleCounter++;
//            }
//			else
//			{
//				_muzzleCounter = 0;
//			}
//			
//			//Shoot again if you have more shots to shoot
//			if(_shots.Count < _shotsPerSalve)
//			{
//	            GameObject shot = (GameObject) Instantiate(_shotPrototype, muzzle.transform.position, muzzle.transform.rotation);
//				_shots.Add(shot);
//	            audio.Play();
//				
//	            //activate selfdestruction
//	            Shot_Script shotScript = shot.GetComponent<Shot_Script>();
//				
//	            if (_target)
//	            {
//					//2 Levels up for all ships. to get coordinates of root gameobject
//					shotScript._parent = gameObject;
//	                shotScript.setTarget(_target);
//					shotScript.shoot();
//					_hasShot = true;
//	            }
//			}
//			else
//				reset();
//        }
//		
//		//Check if shots are still travelling
//		if(_shots != null){
//			
//			if(_shots.Count != 0 || _isTurning || _ready2fire){
//				_isAttacking = true;
//			} else {
//				_isAttacking = false;
//				
//			}
//		}
        
	}

    public void setTarget(GameObject target)
    {
        _isTurning = true;
        _target = target;
        if (target)
        {
            Vector3 targetDirection = target.transform.position - transform.position;
            _turretDestinationRotation = Quaternion.LookRotation(targetDirection);
        }
    }

   
	
	public void doAttack(){
		
		//abandon attack if target is gone
		if(_target){
			
			if(_isTurning){
				turn();
			}
			
				
			if(_isFiring){
				
				_timeGone += Time.deltaTime;
				
				if(_timeGone > 0.5f){
					_timeGone = 0;
					attack();
				}
				
			}
			
			if(_isWaitingForShotsToExpire){
				if(_shotsTravelling <= 0){
					_isAttacking = false;
					_hasShot = true;
				}	
			}
		} else {
			_isAttacking = false;
			_hasShot = true;
		}
		
	}
	
	private void turn(){
		transform.rotation = Quaternion.Slerp(transform.rotation, _turretDestinationRotation, 5f*Time.deltaTime);
	            
		//sufficiant correct angle reached
        if (Quaternion.Angle(_turretDestinationRotation, transform.rotation) <= 1.0f)
        {
            _isTurning = false;
			_isFiring = true;
        }
	}
	
	private void attack(){
		GameObject muzzle = _muzzles[_muzzleCounter] as GameObject;
		
		//Go modulo through muzzles
        if (_muzzleCounter < _muzzleCount-1){
            _muzzleCounter++;
		} else {
			_muzzleCounter = 0;
		}
		
		//Shoot again if you have more shots to shoot
		if((_shotsTotal < _shotsPerSalve) && _target)
		{
            GameObject shot = (GameObject) Instantiate(_shotPrototype, muzzle.transform.position, muzzle.transform.rotation);
			shot.GetComponent<Shot_Script>()._origin = this;
			
            audio.Play();
			
            //activate selfdestruction
            Shot_Script shotScript = shot.GetComponent<Shot_Script>();
			
            if (_target)
            {
				//2 Levels up for all ships. to get coordinates of root gameobject
				shotScript._parent = gameObject;
                shotScript.setTarget(_target);
				shotScript.shoot();
				_shotsTravelling++;
				_shotsTotal++;
            }
		} else {
			_isFiring = false;
			_isWaitingForShotsToExpire = true;
		}
	}
	
	public void reset()
	    {
			_shotsTotal = 0;
			_hasShot = false;
			_isFiring = false;
			_isWaitingForShotsToExpire = false;
	        _isTurning = false;
	        _ready2fire = false;
			_shots.Clear();
	    }
		
	public void decrementShotCount(){
		_shotsTravelling--;	
	}
}
