using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShipStateManager_Script : BBTouchableObject {
	
	//The first state any ship enters, right after being instantiated
	public const int BUY_INSTANTIATED = 0;
	
	//The ship has moved to its starting position (right after instantiation)
	public const int BUY_BOUGHT = 1;
	
	//The ship has been sold and is on its way out
	public const int BUY_SOLD = 2;
	
	//The ship is ready to be moved or pick a fight
	public const int FIGHT_ENABLED = 3;
	
	//A user has touched the ship
	public const int FIGHT_SELECTED = 4;
	
	//While a ship is selected the user hovers over an enemy ship
	public const int FIGHT_TARGETING = 5;
	
	//While an enemy ship is being targeted the user releases the touch
	public const int FIGHT_ATTACKING = 6;
	
	//This ship is currently targeted (only for inactive player)
	public const int FIGHT_TARGETED = 7;
	
	//This ship is currently attacked (only for inactive player)
	public const int FIGHT_ATTACKED = 8;
	
	//The ship is moving to a destination
	public const int FIGHT_MOVING = 9;
	
	//The ship has moved and arrived at its destination
	public const int FIGHT_MOVED = 10;
	
	//The ship is incapable to do any more actions (moving/attacking)
	public const int FIGHT_DISABLED = 11;
	
	//The ship is no longer
	public const int FIGHT_DESTROYED = 12;
	
	//The ship has moved and is now selected;
	public const int FIGHT_MOVED_SELECTED = 13;
	
	//The ship has moved and is now targeting
	public const int FIGHT_MOVED_TARGETING = 14;
	
	
	public int _currentState = BUY_INSTANTIATED;
		
	private PlayerData_Script _playerDataScript;
//	private HUD_Phase1_Script _hudP1_Script;
	private ShipControl_Script _shipControl_Script;
	private Attributes_Script _attr_Script;
	private Camera3rdPerson_Script _cam;
		
	Vector2 _moveTo = new Vector2(0,0);
	
	BBTouchEvent _currentTouch = null;
	
	private bool _targetFound = false;
	private bool _targetInReach = false;
	private Vector3 _attackingPos = new Vector3(0,0,0);
	private bool _attackEnduring = false;
//	private float _timeGone = 0.0f;
	
	private List<TurretControl_Script>  _turrets;

	// Use this for initialization
	void Start () {
		
		_shipControl_Script = this.GetComponent<ShipControl_Script>();
		_attr_Script = this.GetComponent<Attributes_Script>();
		_cam = GameObject.Find("Camera").GetComponent<Camera3rdPerson_Script>(); 
		
		GameObject hud = GameObject.Find("HUD_P" + _attr_Script._player);
		_playerDataScript = hud.GetComponent<PlayerData_Script>();
//		_hudP1_Script = hud.GetComponent<HUD_Phase1_Script>();
		
		//Instantiate List of turrets
		_turrets = new List<TurretControl_Script>();
		
		//Find turrets on this ship (gameObject) and add them to the list
		TurretControl_Script[] turrets = GetComponentsInChildren<TurretControl_Script>();
		foreach(TurretControl_Script turret in turrets)
		{
			_turrets.Add(turret);
		}
						
		switch(_currentState){
		case BUY_INSTANTIATED:
			
//			//If remaining costs are more than 0 or less, ship is instantly deleted again
			if(_playerDataScript._credits == 0){
				_shipControl_Script._sms._playerStates[_shipControl_Script._atrScript._player-1]
					= PlayerState_Manager_Script.PlayerState.BUY_NO_CREDITS;
			}
			
			break;
		}
	}
	
	void Awake () {
		
	}
	
	// Update is called once per frame
	void Update () {
		switch(_currentState){
		case BUY_INSTANTIATED:
			//set the destination of the ship and move it there
			_shipControl_Script.moveTo(_shipControl_Script._destination);
			_shipControl_Script.move();
			
			//When the ship stops moving switch to state Buy_Bought
			if(!_shipControl_Script._move){
				_currentState = BUY_BOUGHT;
			}
		
			break;
		
		case BUY_BOUGHT:
			//determine current PlayerState
			PlayerState_Manager_Script.PlayerState ps = 
				_shipControl_Script._sms._playerStates[_shipControl_Script._atrScript._player-1];
			
			if(ps == PlayerState_Manager_Script.PlayerState.FIGHT_ENABLED){
				_currentState = FIGHT_ENABLED;
			} else if(ps == PlayerState_Manager_Script.PlayerState.FIGHT_DISABLED){
				_currentState = FIGHT_DISABLED;
			}
			
			break;
			
		case BUY_SOLD:
			
			_shipControl_Script.moveTo(_attr_Script._instantiationCoord);
			_shipControl_Script.move();
			
			if(!this.gameObject.renderer.isVisible){
				_playerDataScript._credits += _shipControl_Script._atrScript._costs;
				Destroy(this.gameObject);
			}
			
			break;
		case FIGHT_ENABLED:			
			if(_shipControl_Script._atrScript._player == 1){
				if(_shipControl_Script._sms._inactiveShips[0].Contains(this.gameObject))
					_shipControl_Script._sms._inactiveShips[0].Remove(this.gameObject);
				
				if(!_shipControl_Script._sms._activeShips[0].Contains(this.gameObject))
					_shipControl_Script._sms._activeShips[0].Add(this.gameObject);
			} else {
				if(_shipControl_Script._sms._inactiveShips[1].Contains(this.gameObject))
					_shipControl_Script._sms._inactiveShips[1].Remove(this.gameObject);
				
				if(!_shipControl_Script._sms._activeShips[1].Contains(this.gameObject))
					_shipControl_Script._sms._activeShips[1].Add(this.gameObject);
			}
			
			_shipControl_Script.switchStatusColor(true);
			_shipControl_Script._sms.checkTurnEnd(_shipControl_Script._atrScript._player);
			_shipControl_Script.markAsMoved(false);
			
			Warp_Script warpScript = GetComponent<Warp_Script>();
			warpScript.setWarpPoint(transform.position, transform.rotation);
						
			if(_attr_Script._currentHealth <= 0){
				_currentState = FIGHT_DESTROYED;
			}
			
			break;
			
		case FIGHT_DISABLED:
			
			if(_shipControl_Script._atrScript._player == 1){
				if(_shipControl_Script._sms._activeShips[0].Contains(this.gameObject))
					_shipControl_Script._sms._activeShips[0].Remove(this.gameObject);
				
				if(!_shipControl_Script._sms._inactiveShips[0].Contains(this.gameObject))
					_shipControl_Script._sms._inactiveShips[0].Add(this.gameObject);
			} else {
				if(_shipControl_Script._sms._activeShips[1].Contains(this.gameObject))
					_shipControl_Script._sms._activeShips[1].Remove(this.gameObject);
				
				if(!_shipControl_Script._sms._inactiveShips[1].Contains(this.gameObject))
					_shipControl_Script._sms._inactiveShips[1].Add(this.gameObject);
			}
					
			_shipControl_Script.switchStatusColor(false);
			_shipControl_Script.resetAttack();
			_shipControl_Script.stopEngines();
			_shipControl_Script.markAsMoved(false);
			
			
			_shipControl_Script._sms.checkTurnEnd(_shipControl_Script._atrScript._player);
						
			_shipControl_Script.selectAsTarget(false);
			
			if(_attr_Script._currentHealth <= 0){
				_currentState = FIGHT_DESTROYED;
			}
			
			break;
		case FIGHT_MOVING:
			
			_shipControl_Script.move();
			
			if(!_shipControl_Script._move){
				_currentState = FIGHT_MOVED;
			}
			
			if(_attr_Script._currentHealth <= 0){
				_currentState = FIGHT_DESTROYED;
			}
			
			break;
			
		case FIGHT_MOVED:
			_shipControl_Script.markAsMoved(true);
						
			PlayerState_Manager_Script.PlayerState pState = 
				_shipControl_Script._sms._playerStates[_shipControl_Script._atrScript._player-1];
			
			if(pState == PlayerState_Manager_Script.PlayerState.FIGHT_DISABLED)
				_currentState = FIGHT_DISABLED;
			
			
					
			break;
			
		case FIGHT_TARGETING:
			
			
			GameObject target = _shipControl_Script._targetedShip;
			
			
			//Check what the RayCast hits
			RaycastHit hit = new RaycastHit();
			Vector2 targetPos = new Vector2(_currentTouch.lastScreenPosition.x, _currentTouch.lastScreenPosition.y);
			Ray ray = Camera.main.ScreenPointToRay(targetPos);
			
			if (Physics.Raycast(ray, out hit)){
											
				//Check if a ship is hit
				if (hit.collider.gameObject.tag.Equals("Ship")){
										
					//Check if the object hit is NOT the originally targeted ship
					if((target != null) && (hit.collider.gameObject != target)){
						_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentState = ShipStateManager_Script.FIGHT_DISABLED;
						print("old target: " + target.name);
						print("new target: " + hit.collider.gameObject.name);
					}
					target = hit.collider.gameObject;
					_shipControl_Script._targetedShip = target;
									
					//Check if the ship belongs to the enemy
					if(target.GetComponent<Attributes_Script>()._player != _shipControl_Script._atrScript._player){
						
						_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentState = FIGHT_TARGETED;
						_targetFound = true;
					} else {
						_targetFound = false;
					}
				} else {
					_targetFound = false;
				}
			} else {
				_targetFound = false;
			}
			
			if(!_targetFound){
				print("target lost");
				
				//Set enemy ship as no longer targeted
				if(target != null){
					_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentState = FIGHT_DISABLED;
					_shipControl_Script._targetedShip.GetComponent<ShipControl_Script>().selectAsTarget(false);
				}
				
				_currentState = FIGHT_SELECTED;
			}
			
			if(_attr_Script._currentHealth <= 0){
				_currentState = FIGHT_DESTROYED;
			}
			
			break;
			
		case FIGHT_MOVED_TARGETING:
			
			
			_shipControl_Script.markAsMoved(false);
			GameObject target2 = _shipControl_Script._targetedShip;
			
			//Check what the RayCast hits
			RaycastHit hit2 = new RaycastHit();
			Vector2 targetPos2 = new Vector2(_currentTouch.lastScreenPosition.x, _currentTouch.lastScreenPosition.y);
			Ray ray2 = Camera.main.ScreenPointToRay(targetPos2);
			
			if (Physics.Raycast(ray2, out hit2)){
											
				//Check if a ship is hit
				if (hit2.collider.gameObject.tag.Equals("Ship")){
										
					//Check if the object hit is NOT the originally targeted ship
					if((target2 != null) && (hit2.collider.gameObject != target2)){
						_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentState = ShipStateManager_Script.FIGHT_DISABLED;
						print("old target: " + target2.name);
						print("new target: " + hit2.collider.gameObject.name);
					}
					target2 = hit2.collider.gameObject;
					_shipControl_Script._targetedShip = target2;
									
					//Check if the ship belongs to the enemy
					if(target2.GetComponent<Attributes_Script>()._player != _shipControl_Script._atrScript._player){
						
						_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentState = FIGHT_TARGETED;
						_targetFound = true;
					} else {
						_targetFound = false;
					}
				} else {
					_targetFound = false;
				}
			} else {
				_targetFound = false;
			}
			
			if(!_targetFound){
				print("target lost");
				
				//Set enemy ship as no longer targeted
				if(target2 != null){
					_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentState = FIGHT_DISABLED;
					_shipControl_Script._targetedShip.GetComponent<ShipControl_Script>().selectAsTarget(false);
				}
				
				_currentState = FIGHT_MOVED_SELECTED;
			}
			
			if(_attr_Script._currentHealth <= 0){
				_currentState = FIGHT_DESTROYED;
			}
			
			break;
			
		case FIGHT_TARGETED:
			_shipControl_Script.selectAsTarget(true);
			
			if(_attr_Script._currentHealth <= 0){
				_currentState = FIGHT_DESTROYED;
			}
			
			break;
			
		case FIGHT_ATTACKING:
			
			_shipControl_Script._sms._fadeInHealthbars = true;			
			if(_targetInReach){
				//Fly to position from which to attack
				if(!_attackEnduring){
					_shipControl_Script.moveTo(_attackingPos);
					_shipControl_Script.move();
				}
				
				
				//Ship arrived at its attacking Position
				if(!_shipControl_Script._move){
										
					//Start attacking
//					if(!_attackEnduring){
						_shipControl_Script.attack();
//					}
					
					_attackEnduring = _shipControl_Script.checkTurretsAttacking();
//					print("Fight_ATTACK: attack enduring: " + _attackEnduring);
					
//					print(_attackEnduring);
					if(!_attackEnduring){
						_shipControl_Script._sms._fadeInHealthbars = false;
						_currentState = FIGHT_DISABLED;
						
						if(_shipControl_Script._targetedShip){
							_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentState = FIGHT_DISABLED;
						}
						
						//After the attack is before the attack...
						foreach(TurretControl_Script turret in _turrets)
						{
//							turret._hasShot = false;
							turret.reset();
						}
						
					}

				}
			} else if(!_attackEnduring){
				_currentState = FIGHT_ENABLED;
			}
						
			if(_attr_Script._currentHealth <= 0){
				_currentState = FIGHT_DESTROYED;
			}
			
			break;
		
		
		case FIGHT_ATTACKED:
			
			_shipControl_Script._sms._fadeInHealthbars = true;
			if(_attr_Script._currentHealth <= 0){
				_currentState = FIGHT_DESTROYED;
			}
			
			break;
		
		case FIGHT_DESTROYED:
			
			if(_shipControl_Script._atrScript._player == 1){
				if(_shipControl_Script._sms._activeShips[0].Contains(this.gameObject))
					_shipControl_Script._sms._activeShips[0].Remove(this.gameObject);
				if(_shipControl_Script._sms._inactiveShips[0].Contains(this.gameObject))
					_shipControl_Script._sms._inactiveShips[0].Remove(this.gameObject);
			} else {
				if(_shipControl_Script._sms._activeShips[1].Contains(this.gameObject))
					_shipControl_Script._sms._activeShips[1].Remove(this.gameObject);
				if(_shipControl_Script._sms._inactiveShips[1].Contains(this.gameObject))
					_shipControl_Script._sms._inactiveShips[1].Remove(this.gameObject);
			}
			
			_shipControl_Script._sms.checkTurnEnd(_shipControl_Script._atrScript._player);
			_shipControl_Script._sms.checkEndGame(_shipControl_Script._atrScript._player);
			
			GameObject _explosion = (GameObject) Instantiate(
				_shipControl_Script._explosionPrototype, gameObject.transform.position, gameObject.transform.rotation);
			
			//GameObject _soundExplosion = (GameObject) Instantiate(_explosion, _cam._currentCamPos + new Vector3(0,-2,0), _cam._currentCamOrient);
			
			GameObject _soundExplosion = (GameObject) Instantiate(_shipControl_Script._soundExplosionPrototype, _cam._currentCamPos + new Vector3(0,5,0), _cam._currentCamOrient);
			
			_explosion.GetComponent<Detonator>().Explode();
			_soundExplosion.GetComponent<Detonator>().Explode();
			
			_shipControl_Script.splashExplode();
			
			Destroy(this.gameObject);
			
			break;
			
//		case FIGHT_SELECTED:
//			break;
		
		}
		
		
	}
	
	public override void handleSingleTouch(BBTouchEvent aTouch){
		_shipControl_Script._sms._fadeInHealthbars = true;
		switch(_currentState){
//		case BUY_BOUGHT:
//			//determine current PlayerState
//			PlayerState_Manager_Script.PlayerState ps = 
//				_shipControl_Script._sms._playerStates[_shipControl_Script._atrScript._player-1];
//			
//			if((ps == PlayerState_Manager_Script.PlayerState.BUY_NO_CREDITS) || 
//				(ps == PlayerState_Manager_Script.PlayerState.BUY_SELECTION))
//				_currentState = BUY_SOLD;
//			
//			break;
			
		case FIGHT_ENABLED:
			if(!_playerDataScript._warpModeActive){
				_currentState = FIGHT_SELECTED;
			}
			
			break;
			
		case FIGHT_MOVED:
			if(!_playerDataScript._warpModeActive){
				_currentState = FIGHT_MOVED_SELECTED;
			}
			
			break;
		
		case FIGHT_SELECTED:
						
			//Check what the RayCast hits
			RaycastHit hit = new RaycastHit();
			_moveTo = new Vector2(aTouch.lastScreenPosition.x, aTouch.lastScreenPosition.y);
			Ray ray = Camera.main.ScreenPointToRay(_moveTo);
			
			if(!_shipControl_Script._selected){
				_shipControl_Script.select(true);
			}
			
			if (Physics.Raycast(ray, out hit)){
												
				//Check if a ship is hit
				if (hit.collider.gameObject.tag.Equals("Ship")){
					_shipControl_Script._targetedShip = hit.collider.gameObject;
										
					//Check if the ship belongs to the enemy
					if(_shipControl_Script._targetedShip.GetComponent<Attributes_Script>()._player != _shipControl_Script._atrScript._player){
												
						//Set this ship as targeting
						_currentState = FIGHT_TARGETING;
						_targetFound = true;
						
						_currentTouch = aTouch;
						//_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentTouch = aTouch;
						
					}	
				} 
				
			} else {
				
				//If no ship has been raycasted yet, nothing happens
				//else that ship is set to disabled
				if(_shipControl_Script._targetedShip != null){
					_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentState = FIGHT_DISABLED;
				}
			}
			
			//_shipControl_Script.
			_shipControl_Script.showAttackRadius(_moveTo);
		
			break;
			
		case FIGHT_TARGETING:
			_currentTouch = aTouch;
			
			break;
		
		case FIGHT_MOVED_SELECTED:
			_shipControl_Script.markAsMoved(false);
			
			//Check what the RayCast hits
			RaycastHit hit2 = new RaycastHit();
			_moveTo = new Vector2(aTouch.lastScreenPosition.x, aTouch.lastScreenPosition.y);
			Ray ray2 = Camera.main.ScreenPointToRay(_moveTo);
						
			if (Physics.Raycast(ray2, out hit2)){
												
				//Check if a ship is hit
				if (hit2.collider.gameObject.tag.Equals("Ship")){
					_shipControl_Script._targetedShip = hit2.collider.gameObject;
										
					//Check if the ship belongs to the enemy
					if(_shipControl_Script._targetedShip.GetComponent<Attributes_Script>()._player != _shipControl_Script._atrScript._player){
												
						//Set this ship as targeting
						_currentState = FIGHT_MOVED_TARGETING;
						_targetFound = true;
						
						_currentTouch = aTouch;
						//_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentTouch = aTouch;
						
					}	
				} 
				
			} else {
				
				//If no ship has been raycasted yet, nothing happens
				//else that ship is set to disabled
				if(_shipControl_Script._targetedShip != null){
					_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentState = FIGHT_DISABLED;
				}
			}
			
			//_shipControl_Script.
			_shipControl_Script.showStaticAttackRadius();
			
			break;
			
		case FIGHT_MOVED_TARGETING:
			
			_shipControl_Script.markAsMoved(false);
			
			_currentTouch = aTouch;
			
			break;
		}
	}
	
	public override void noTouches(){
						
		switch(_currentState){
		case FIGHT_SELECTED:
						
			_shipControl_Script.moveTo(_moveTo);
			
			_shipControl_Script.select(false);
			_shipControl_Script.hideAttackRadius();
			
			_currentState = FIGHT_MOVING;
			_shipControl_Script._sms._fadeInHealthbars = false;
			
			break;
			
		case FIGHT_MOVED_SELECTED:
						
			_shipControl_Script.moveTo(_moveTo);
			
			_shipControl_Script.select(false);
			_shipControl_Script.hideAttackRadius();
			
			_currentState = FIGHT_MOVED;
			_shipControl_Script._sms._fadeInHealthbars = false;
			
			break;
			
		case FIGHT_TARGETING:
			Vector3 posTarget = new Vector3(0,0,0);
			Vector3 posAttacker = new Vector3(0,0,0);
			//Vector3 direction = new Vector3(0,0,0);
			float distAttackerToTarget = 0;
			float distAttackerToAttackRadiusEdge = 0;
			
			//Estimate Position of attacker and target and direction
			posAttacker = this.gameObject.transform.position;
			if(_shipControl_Script._targetedShip){
				posTarget = _shipControl_Script._targetedShip.transform.position;
			}
			
			
			//Estimate Distances
			distAttackerToTarget = Vector3.Distance(posAttacker, posTarget);
			distAttackerToAttackRadiusEdge = _attr_Script._maxMovementDistance + _attr_Script._attackRadius;
			
			_shipControl_Script.hideAttackRadius();
			_shipControl_Script.select(false);
			_shipControl_Script._targetedShip.GetComponent<ShipControl_Script>().selectAsTarget(false);
			
			//If enemy is in reach go to state ATTACKING
			if(distAttackerToTarget <= distAttackerToAttackRadiusEdge){
				print("targeting: target in reach");
				_targetInReach = true;
				
				if(distAttackerToTarget > _attr_Script._attackRadius){
					//Calculate Position to fly to
					_attackingPos = posTarget-posAttacker;
					_attackingPos.Normalize();				
					_attackingPos *= distAttackerToTarget-_attr_Script._attackRadius;
					_attackingPos += posAttacker;
				} else {
					_attackingPos = transform.position;
				}
				
				_currentState = FIGHT_ATTACKING;
				_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentState = FIGHT_ATTACKED;
			} else {
				_shipControl_Script._sms._fadeInHealthbars = false;
				_currentState = FIGHT_ENABLED;
				_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentState = FIGHT_DISABLED;
			}
			
			
			break;
			
		case FIGHT_MOVED_TARGETING:
			Vector3 posTarget2 = new Vector3(0,0,0);
			Vector3 posAttacker2 = new Vector3(0,0,0);
			float distAttackerToTarget2 = 0;
			
			//Estimate Position of attacker and target and direction
			posAttacker2 = this.gameObject.transform.position;
			if(_shipControl_Script._targetedShip){
				posTarget2 = _shipControl_Script._targetedShip.transform.position;
			}
			
			
			//Estimate Distances
			distAttackerToTarget2 = Vector3.Distance(posAttacker2, posTarget2);
			
			_shipControl_Script.hideAttackRadius();
			_shipControl_Script.select(false);
			_shipControl_Script._targetedShip.GetComponent<ShipControl_Script>().selectAsTarget(false);
			
			//If enemy is in reach go to state ATTACKING
			if(distAttackerToTarget2 <= _attr_Script._attackRadius){
				print("targeting: target in reach");
				_targetInReach = true;
				_attackingPos = transform.position;
				
				_currentState = FIGHT_ATTACKING;
				_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentState = FIGHT_ATTACKED;
			} else {
				_shipControl_Script._sms._fadeInHealthbars = false;
				_currentState = FIGHT_MOVED;
				_shipControl_Script._targetedShip.GetComponent<ShipStateManager_Script>()._currentState = FIGHT_DISABLED;
			}
			
			
			break;
		
		}
			
	}
}
