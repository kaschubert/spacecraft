using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class ShipControl_Script : MonoBehaviour
{
	public PlayerState_Manager_Script 		_sms;
	
	public GameObject explSphere;
	
    public bool                			_selected = false;

    private Ellipse_Script      		_movementMarker_Script;
    public  Ellipse_Script      		_positionMarker_Script;
    private Ellipse_Script      		_attackArea_Script;
	
//    private MeshRenderer        		_movementMarker_Renderer;
    public  MeshRenderer        		_positionMarker_Renderer;
	public Texture						_positionMarkerMovedTex;
	public Texture						_positionMarkerAttackedTex;
//    private MeshRenderer        		_attackMarker_Renderer;

    private Quaternion          		_destinationRotation;
    private GameObject          		_selectSoundEffect;
    private GameObject          		_releaseSoundEffect;
    private GameObject          		_attackSoundEffect;
    public Vector3              		_destination;
    public bool                 		_move;
    private Vector3             		_oldHitPoint;
    
    public Attributes_Script   			_atrScript;
//	private GUI_Script 					_guiScript;
	
	public bool                        _isAttacking;
    private GameObject                  _turretTarget;
    protected List<TurretControl_Script>  _turrets;
    public AttackMarker_Script          _attackMarker_Script;

    public GameObject                  	_explosionPrototype;
	public GameObject					_soundExplosionPrototype;
    protected List<GameObject>          _engineBlast;
	
	public GameObject					_targetedShip;
	
	public Camera 						_renderingCamera;
	private Camera3rdPerson_Script		_cameraScript;
	
	public Color						_teamColor;
	public Color 						_activeColor = new Color(1,1,1,1);
	public Color 						_inactiveColor = new Color(0.4F,0.4F,0.4F,1);
	
	public Material						_shipMat;
	public Material						_gravBlockMat;
	
	protected virtual void Start4Derived()
	{
	}

    void Start()
    {
		_engineBlast = new List<GameObject>();
		_turrets = new List<TurretControl_Script>();
		
		TurretControl_Script[] turrets = GetComponentsInChildren<TurretControl_Script>();
		
		foreach(TurretControl_Script turret in turrets)
		{
			_turrets.Add(turret);
		}
		
		_cameraScript = _renderingCamera.GetComponent<Camera3rdPerson_Script>();
		
        _atrScript = GetComponent<Attributes_Script>();
//		_guiScript = GameObject.Find("GUI").GetComponent<GUI_Script>();
        
        GameObject movementMarker = gameObject.transform.Find("_Movement_Marker").gameObject;
        _movementMarker_Script = movementMarker.GetComponent<Ellipse_Script>();
//        _movementMarker_Renderer = movementMarker.GetComponentInChildren<MeshRenderer>();

        GameObject positionMarker = gameObject.transform.Find("_Position_Marker").gameObject;
        _positionMarker_Script = positionMarker.GetComponent<Ellipse_Script>();
        _positionMarker_Renderer = positionMarker.GetComponentInChildren<MeshRenderer>();

         GameObject attackMarker = gameObject.transform.Find("_Attack_Marker").gameObject;
        _attackArea_Script = attackMarker.GetComponent<Ellipse_Script>();
//        _attackMarker_Renderer = attackMarker.GetComponentInChildren<MeshRenderer>();
        _attackMarker_Script = attackMarker.GetComponent<AttackMarker_Script>();
        
        _selectSoundEffect = GameObject.Find("Select_SoundEffect");
        _releaseSoundEffect = GameObject.Find("Release_SoundEffect");
        _attackSoundEffect = GameObject.Find("Attack_SoundEffect");

        _movementMarker_Script.recalculateEllipse(_atrScript._maxMovementDistance, _atrScript._maxMovementDistance);

        _attackArea_Script.recalculateEllipse(_atrScript._attackRadius, _atrScript._attackRadius);

        SphereCollider attackSphere = _attackMarker_Script.gameObject.GetComponent<SphereCollider>();
        attackSphere.radius = _atrScript._attackRadius;

        #if true
		
        Vector3 clearedPosition;
        GameObject markerParent = _positionMarker_Script.gameObject;
        clearedPosition = markerParent.transform.position;
        clearedPosition.y = -1;
        markerParent.transform.position = clearedPosition;
        
		markerParent = _attackMarker_Script.gameObject;
		clearedPosition = markerParent.transform.position;
		clearedPosition.y = -2;
		markerParent.transform.position = clearedPosition;
        
		markerParent = _movementMarker_Script.gameObject;
		clearedPosition = markerParent.transform.position;
		clearedPosition.y = -3;
		markerParent.transform.position = clearedPosition;
        
        #endif
		
		_renderingCamera = Camera.main;
		
		_sms = GameObject.Find("GameLogic").GetComponent<PlayerState_Manager_Script>();
		
		Start4Derived();
	}
	
	void Awake(){
		_atrScript = GetComponent<Attributes_Script>();
//		_guiScript = GameObject.Find("GUI").GetComponent<GUI_Script>();
		_sms = GameObject.Find("GameLogic").GetComponent<PlayerState_Manager_Script>();
		_renderingCamera = Camera.main;
	}
	
	void Update()
    {
		Update4Derived();
	}
	
	protected virtual void Update4Derived()
	{
	}
	
	//Displays the Radius of this ship wherein it can attack enemy units
    public void showAttackRadius(Vector2 hitPoint2D)
    {
		Vector3 correctedHit3D = getPosInMaxMoveRadFrom2D(hitPoint2D);
		
        if (Vector3.Distance(_oldHitPoint, correctedHit3D) > 1.5f)
        {
			GameObject markerParent = _attackMarker_Script.gameObject;	
			markerParent.transform.position = correctedHit3D;

            _attackArea_Script.show();
			
            //to activate the collider attached
            _attackArea_Script.gameObject.active = true;
        }
		
		_oldHitPoint = correctedHit3D;
    }
	
	public void showStaticAttackRadius()
    {
		GameObject markerParent = _attackMarker_Script.gameObject;
		markerParent.transform.position = transform.position;
    	_attackArea_Script.show();
		_attackArea_Script.gameObject.active = true;
    }
	
	//Hides the Radius of this ship wherein it can attack enemy units
    public void hideAttackRadius()
    {
        _attackArea_Script.hide();
        ////to deactivate the collider attached
        _attackArea_Script.gameObject.active = false;
    }

    public void select(bool really)
    {
        _selected = really;
		
		Color targetColor = new Color(1.0f, 0.88f, 0.0f, 0.2f);
		_positionMarker_Renderer.material.color = targetColor;
//		_positionMarker_Renderer.material.SetColor("_MainColor", targetColor);
		_positionMarker_Renderer.material.mainTexture = _positionMarkerMovedTex;

        if (really)
        {
//            _positionMarker_Script.show();
            _movementMarker_Script.show();
            _selectSoundEffect.audio.Play();
        }
        else
        {
//            _positionMarker_Script.hide();
            _movementMarker_Script.hide();
            _releaseSoundEffect.audio.Play();
        }
    }
	
	public void markAsMoved(bool moved){
		if (moved){
            _positionMarker_Script.show();
        } else {
            _positionMarker_Script.hide();
        }
	}

    public void selectAsTarget(bool really)
    {			
		Color targetColor = new Color(1.0f, 0.0f, 0.0f, 0.15f);
		_positionMarker_Renderer.material.color = targetColor;
		_positionMarker_Renderer.material.mainTexture = _positionMarkerAttackedTex;
		
		if (really)
        {
//			print("showing");
            _positionMarker_Script.show();
        }
        else
        {
//			print("hiding");
            _positionMarker_Script.hide();
        }
    }
	
	public bool checkTurretsAttacking(){
		bool attackEnduring = true;
//		print("entered checkmeth");
		if(_turrets != null){
//			print("turrets existant");
			//make sure every turret has fired and his shots have vanished
	        foreach (TurretControl_Script turret in _turrets)
	        {	
	            if(turret._hasShot){
//					print("turret: _hasShot: " + turret._hasShot);
					if(!turret._isAttacking){
						attackEnduring = false;
//						print("turret: _isAttacking: " + turret._isAttacking);
					} else {
						attackEnduring = true;
//						print("turret: _isAttacking: " + turret._isAttacking);
						break;
					}
				} else{
					attackEnduring = true;
//					print("lower enduring");
					break;
				}
				
	        }
		}
		
		return attackEnduring;
	}
	
	protected virtual void attack4Derived()
	{
		_turretTarget = _targetedShip;
		
		if(_turrets != null){
			
	        foreach (TurretControl_Script turret in _turrets)
	        {	
//				if(!_isAttacking){
	            	turret.setTarget(_turretTarget);
//				}
				turret.doAttack();
	        }
		}
	}
	
	public void attack()
    {		
		if(!_isAttacking){
			_attackSoundEffect.audio.Play();
		}
    	_isAttacking = true;
   		attack4Derived();
    }
	
	//Sets the destination this ship will move to (from 2D coords)
	public void moveTo(Vector2 destination2D)
    {		
		Vector3 destination3D = getPosInMaxMoveRadFrom2D(destination2D);
		moveTo(destination3D);
    }
	
	//Sets the destination this ship will move to
    public void moveTo(Vector3 destination3D)
    {
		resetAttack();
		
		foreach(GameObject blast in _engineBlast)
		{
			blast.particleEmitter.emit = true;
		}
		
		_destination = destination3D;
		
		_move = true;
		
        Vector3 targetDirection = _destination - transform.position;
		
        //LookAt is the freakin magic^^
        _destinationRotation = Quaternion.LookRotation(targetDirection);

        Debug.DrawLine(_destination, transform.position);
    }
	
	//Converts ScreenCoords from the touches to WorldCoords
	public Vector3 getWorldCoords(Vector2 Coords2D){
		
		//Distance that the camera is floating above the scene
		float lookAtDist = _cameraScript._camHeight;
		//Convert the 2D input to 3D coordinates
		Vector3 converted2D = new Vector3(Coords2D.x, Coords2D.y, lookAtDist);
		converted2D = this._renderingCamera.ScreenToWorldPoint(converted2D);
		
		return converted2D;
	}
	
	//calculate a new position, considerung the maximum movement radius of the ship (from ScreenCoords)
	public Vector3 getPosInMaxMoveRadFrom2D(Vector3 check2DCoord){
		
		//Convert the 2D input to 3D coordinates
		Vector3 converted2D = getWorldCoords(check2DCoord);
		
		return getPosInMaxMoveRad(converted2D);
	}
	
	//calculate a new position, considerung the maximum movement radius of the ship (from WorldCoords)
	public Vector3 getPosInMaxMoveRad(Vector3 coords){
		
		//Calculate Distance of touch from the ship's center
        Vector3 mouseTarget = coords - transform.position;
        float dist = mouseTarget.magnitude;
		
        //don't let center of attack marker exceed the radius of the ship's maximum movement radius
        if (dist > _atrScript._maxMovementDistance)
        {
            return (transform.position + mouseTarget.normalized * (_atrScript._maxMovementDistance));
        }
        else
        {
			return coords;
        }
	}
	
	//Gradually move the ship to its new destination and turn of engine blast and movement when destination is reached
	public void move(){
		transform.position = Vector3.Lerp(transform.position, _destination, Time.deltaTime * _atrScript._speed);
        transform.rotation = Quaternion.Slerp(transform.rotation, _destinationRotation, Time.deltaTime);
		float dist = Vector3.Distance(_destination, transform.position);
		
		foreach(GameObject blast in _engineBlast)
		{
			if (dist <= 3 && blast.particleEmitter.emit == true)
            	blast.particleEmitter.emit = false;
		}
		
		if (dist <= 2f){
			_move = false;
		}
	}
	
	//reset the ships turrets to their initial position
	public void resetAttack()
    {
		if(_turrets != null){
			
	        foreach (TurretControl_Script turret in _turrets)
	        {
	            //print("reset" + turret.gameObject.name);
	
	            turret.reset();
				
	        }
	        _isAttacking = false;
		}
    }
	
	public virtual void switchStatusColor(bool active){
	}
	
	public void splashExplode(){
		print("splashExploding");
		GameObject boom = (GameObject)Instantiate(explSphere, transform.position, transform.rotation);
		boom.GetComponent<ExplosionSphere_Script>()._radius = _atrScript._explosionRadius;
		boom.GetComponent<ExplosionSphere_Script>()._damage = _atrScript._maxHealth*0.15f;
	}
	
	public void stopEngines(){	
		foreach(GameObject blast in _engineBlast)
		{
           	blast.particleEmitter.emit = false;
		}
	}
}
