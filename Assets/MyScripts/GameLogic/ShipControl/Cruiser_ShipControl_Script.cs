using UnityEngine;
using System.Collections;

public class Cruiser_ShipControl_Script : ShipControl_Script {
	
	private string[] _turretPaths;
	private string[] _gunPaths;
	private Material[] _turretMats;
	private Material[] _gunMats;

	protected override void Start4Derived()
	{		
		_shipMat = transform.FindChild("Cruiser_lowPoly_model").renderer.material;
		_shipMat.SetColor("_TeamColor", _teamColor);
		_shipMat.SetColor("_MainColor", _activeColor);
		_gravBlockMat = transform.FindChild("Cruiser_lowPoly_model/GravBlock").renderer.material;
		_gravBlockMat.SetColor("_TeamColor", _teamColor);
		
		_turretPaths = new string[4];
		_turretPaths[0] = "Cruiser_lowPoly_model/Turret_Pos_R/Turret_CruiserTop/Turret_Base";
		_turretPaths[1] = "Cruiser_lowPoly_model/Turret_Pos_R/Turret_CruiserBottom/Turret_Base";
		_turretPaths[2] = "Cruiser_lowPoly_model/Turret_Pos_L/Turret_CruiserTop/Turret_Base";
		_turretPaths[3] = "Cruiser_lowPoly_model/Turret_Pos_L/Turret_CruiserBottom/Turret_Base";
		
		_turretMats = new Material[4];
		for(int i = 0 ; i < _turretMats.Length ; i++){
			_turretMats[i] = transform.FindChild(_turretPaths[i]).renderer.material;
		}
		
		_gunPaths = new string[4];
		_gunPaths[0] = "Cruiser_lowPoly_model/Turret_Pos_R/Turret_CruiserTop/Turret_Base/Gun_Ani_U_D/Gun";
		_gunPaths[1] = "Cruiser_lowPoly_model/Turret_Pos_R/Turret_CruiserBottom/Turret_Base/Gun_Ani_U_D/Gun";
		_gunPaths[2] = "Cruiser_lowPoly_model/Turret_Pos_L/Turret_CruiserTop/Turret_Base/Gun_Ani_U_D/Gun";
		_gunPaths[3] = "Cruiser_lowPoly_model/Turret_Pos_L/Turret_CruiserBottom/Turret_Base/Gun_Ani_U_D/Gun";
		
		_gunMats = new Material[4];
		for(int i = 0 ; i < _gunMats.Length ; i++){
			_gunMats[i] = transform.FindChild(_gunPaths[i]).renderer.material;
		}
		
        string path2Engine = "Cruiser_lowPoly_model/Engine/Engine_Blast";
		
		_engineBlast.Add(transform.Find(path2Engine).gameObject);
	}	
	
	public override void switchStatusColor(bool active){
				
		if(active){
			_shipMat.SetColor("_MainColor", _activeColor);
			_gravBlockMat.SetColor("_MainColor", _activeColor);
			
			for(int i = 0 ; i < _turretMats.Length ; i++){
				_turretMats[i].SetColor("_MainColor", _activeColor);
			}
			
			for(int i = 0 ; i < _gunMats.Length ; i++){
				_gunMats[i].SetColor("_MainColor", _activeColor);
			}
			
		} else {
			_shipMat.SetColor("_MainColor", _inactiveColor);
			_gravBlockMat.SetColor("_MainColor", _inactiveColor);
			
			for(int i = 0 ; i < _turretMats.Length ; i++){
				_turretMats[i].SetColor("_MainColor", _inactiveColor);
			}
			
			for(int i = 0 ; i < _gunMats.Length ; i++){
				_gunMats[i].SetColor("_MainColor", _inactiveColor);
			}
			
		}
	}
}
