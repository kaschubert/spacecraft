using UnityEngine;
using System.Collections;

public class Hunter_ShipControl_Script : ShipControl_Script {
	
	private bool turn2Attack;
	private Material _shipMat2;
	private Material _shipMat3;
	private Material _shipMat4;
	
	protected override void Start4Derived()
	{
		_shipMat = transform.FindChild("Hunter_Single_1").renderer.material;
		_shipMat.SetColor("_TeamColor", _teamColor);
		_shipMat.SetColor("_MainColor", _activeColor);
		
		_shipMat2 = transform.FindChild("Hunter_Single_2").renderer.material;
		_shipMat3 = transform.FindChild("Hunter_Single_3").renderer.material;
		_shipMat4 = transform.FindChild("Hunter_Single_4").renderer.material;
		_shipMat2.SetColor("_TeamColor", _teamColor);
		_shipMat3.SetColor("_TeamColor", _teamColor);
		_shipMat4.SetColor("_TeamColor", _teamColor);
		
		string path2Engine;
			
		for(int i = 1 ; i <= 4; i++)
		{
			path2Engine = "Hunter_Single_" + i + "/Engine/Engine_Blast";	
			_engineBlast.Add(transform.Find(path2Engine).gameObject);
		}
	}	
	
	public override void switchStatusColor(bool active){
		if(active){
			_shipMat.SetColor("_MainColor", _activeColor);
			_shipMat2.SetColor("_MainColor", _activeColor);
			_shipMat3.SetColor("_MainColor", _activeColor);
			_shipMat4.SetColor("_MainColor", _activeColor);
		} else {
			_shipMat.SetColor("_MainColor", _inactiveColor);
			_shipMat2.SetColor("_MainColor", _inactiveColor);
			_shipMat3.SetColor("_MainColor", _inactiveColor);
			_shipMat4.SetColor("_MainColor", _inactiveColor);
		}
	}
}
