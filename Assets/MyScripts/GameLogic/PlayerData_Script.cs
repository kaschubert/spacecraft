using UnityEngine;
using System.Collections;

//Script for accumulating data for a player
public class PlayerData_Script : MonoBehaviour {
	public int _player;
	public int _credits = 500;
	public int _globalRound = 1;
	public bool _warpModeActive = false;
	public Color _teamColor;
	
	//currently selected type of ship
	public int _selected;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
