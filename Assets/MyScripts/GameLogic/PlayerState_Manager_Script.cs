using UnityEngine;
using System.Collections;

public class PlayerState_Manager_Script : MonoBehaviour {
	
	public enum PlayerState{
		BUY_SELECTION,
		BUY_NO_CREDITS,
		BUY_LOCKED_IN,
		FIGHT_ENABLED,
		FIGHT_DISABLED,
		FIGHT_WARP,
		WIN,
		LOSE
	}
	
	public HUD_Phase1_Script[] _phase1Scripts = new HUD_Phase1_Script[2];
	public HUD_Phase2_Script[] _phase2Scripts = new HUD_Phase2_Script[2];
	public GameObject healthPackSpawner;
	
	public PlayerState[] _playerStates = new PlayerState[2];
	public ArrayList[] _activeShips = new ArrayList[2];
	public ArrayList[] _inactiveShips = new ArrayList[2];
	public ArrayList _activeShipsP1 = new ArrayList();
	public ArrayList _inactiveShipsP1 = new ArrayList();
	public ArrayList _activeShipsP2 = new ArrayList();
	public ArrayList _inactiveShipsP2 = new ArrayList();
	
	public ArrayList _activeProjectiles = new ArrayList();
	
	private bool _switchToPhase2 = false;
	
	public int _activePlayer = 1;
	public int _inactivePlayer = 2;
	
	private ChooseTimer[] _chooseTimer = new ChooseTimer[2];
//	private float timerTime = 0f;
//	private float winTime = 0f;
	public bool _gameStarted = false;
	
	private float _currentHealthbarAlpha = 0f;
	public bool _fadeInHealthbars = true;	
	
	void Awake () {
		_activeShips[0] = new ArrayList();
		_activeShips[1] = new ArrayList();
		_inactiveShips[0] = new ArrayList();
		_inactiveShips[1] = new ArrayList();
		
		_phase1Scripts[0] = GameObject.Find("HUD_P1").GetComponent<HUD_Phase1_Script>();
		_phase1Scripts[1] = GameObject.Find("HUD_P2").GetComponent<HUD_Phase1_Script>();
		_phase2Scripts[0] = GameObject.Find("HUD_P1").GetComponent<HUD_Phase2_Script>();
		_phase2Scripts[1] = GameObject.Find("HUD_P2").GetComponent<HUD_Phase2_Script>();
		
		_chooseTimer[0] = GameObject.Find("HUD_P1").GetComponent<ChooseTimer>();
		_chooseTimer[1] = GameObject.Find("HUD_P2").GetComponent<ChooseTimer>();
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		if(_fadeInHealthbars){
			fadeInHealthbars();
		} else {
			fadeOutHealthbars();	
		}
		
		if(_gameStarted){
			if(_playerStates[0] != PlayerState_Manager_Script.PlayerState.BUY_LOCKED_IN){
				_chooseTimer[0].startTimer();
			}
			
			if(_playerStates[1] != PlayerState_Manager_Script.PlayerState.BUY_LOCKED_IN){
				_chooseTimer[1].startTimer();
			}	
		}
		
		for(int i = 0 ; i < _playerStates.Length ; i++){
			if(_playerStates[i] == PlayerState.BUY_LOCKED_IN)
				_switchToPhase2 = true;
			else {
				_switchToPhase2 = false;
				break;
			}	
		}
		
		for(int i = 0 ; i < _playerStates.Length ; i++){
			switch(_playerStates[i]){
			case PlayerState.BUY_SELECTION:
				if(_gameStarted){
					_phase1Scripts[i].setGuiActivity(true, HUD_Phase1_Script.ALL_ELEMENTS);
					_phase1Scripts[i].setGuiActivity(false, HUD_Phase1_Script.ONLY_OK);
				}
				break;
			case PlayerState.BUY_NO_CREDITS:
				_phase1Scripts[i].setGuiActivity(true, HUD_Phase1_Script.ONLY_OK);
				break;
			case PlayerState.BUY_LOCKED_IN:
				_phase1Scripts[i].setGuiActivity(false, HUD_Phase1_Script.ALL_ELEMENTS);
				break;	
			case PlayerState.FIGHT_ENABLED:
//				print("player enabled: " + (i+1));
				_phase2Scripts[i].setGuiActivity(true, i+1);
				break;
			case PlayerState.FIGHT_DISABLED:
//				print("player disabled: " + (i+1));
				_phase2Scripts[i].setGuiActivity(false, i+1);
				
				break;
			case PlayerState.WIN:
				_phase2Scripts[i]._matchWon = true;
				_phase2Scripts[i]._gameEnded = true;
				_phase2Scripts[i].setGuiActivity(true, i+1);
				break;
			
			case PlayerState.LOSE:
				_phase2Scripts[i]._matchWon = false;
				_phase2Scripts[i]._gameEnded = true;
				_phase2Scripts[i].setGuiActivity(true, i+1);
				break;
			}	
			
		}
		
		if(_switchToPhase2){
			_playerStates[0] = PlayerState.FIGHT_ENABLED;
			_playerStates[1] = PlayerState.FIGHT_DISABLED;
			_currentHealthbarAlpha = 0;
			_fadeInHealthbars = false;
		}			
	}
	
	public void checkTurnEnd(int player){
		int counterPlayer = player%2; //returns the desired form for the array 
		
		if(_playerStates[player-1] == PlayerState.FIGHT_ENABLED){
			if(_activeShips[player-1].Count == 0){
				
				healthPackSpawner.GetComponent<Spawner_Script>().spawn(new Vector2(-300,300), new Vector2(-200,200));
				
				_playerStates[player-1] = PlayerState_Manager_Script.PlayerState.FIGHT_DISABLED;
				_inactivePlayer = player;
				_playerStates[counterPlayer] = PlayerState_Manager_Script.PlayerState.FIGHT_ENABLED;
				_activePlayer = counterPlayer;
				
				foreach(GameObject ship in _inactiveShips[counterPlayer]){
					if(ship){
						ship.GetComponent<ShipStateManager_Script>()._currentState = ShipStateManager_Script.FIGHT_ENABLED;
					}
				}
				
			}
		}
	}
	
	public void checkEndGame(int player){
		player = (player+1)%2; //returns the desired form for the array 
		int counterPlayer = (player+1)%2; //returns the desired form for the array 	
		
//		print("checkGameEnd");
		if(_inactiveShips[player].Count == 0 && _activeShips[player].Count == 0){
			_activeShips[counterPlayer].Clear();
			_inactiveShips[counterPlayer].Clear();
			_activeShips[player].Clear();
			_inactiveShips[player].Clear();
			_playerStates[player] = PlayerState_Manager_Script.PlayerState.LOSE;
			_playerStates[counterPlayer] = PlayerState_Manager_Script.PlayerState.WIN;
		} else if(_inactiveShips[counterPlayer].Count == 0 && _activeShips[counterPlayer].Count == 0) {
			_activeShips[counterPlayer].Clear();
			_inactiveShips[counterPlayer].Clear();
			_activeShips[player].Clear();
			_inactiveShips[player].Clear();
			_playerStates[player] = PlayerState_Manager_Script.PlayerState.WIN;
			_playerStates[counterPlayer] = PlayerState_Manager_Script.PlayerState.LOSE;
		}
	}
	
	//Switch active and inactive player
	public void forceTurnEnd(){
		int activePlayer = 0;
		
		//Determine active Player
		for(int i = 0; i < _playerStates.Length ; i++){
			if((_playerStates[i] == PlayerState.FIGHT_ENABLED) || (_playerStates[i] == PlayerState.FIGHT_WARP)){
				
				activePlayer = i;
			}
		}	
		
		//Determine counterPlayer
		int counterPlayer = (activePlayer+1)%2;
		
		//Switch playerStates
		_playerStates[counterPlayer] = PlayerState_Manager_Script.PlayerState.FIGHT_ENABLED;
		_activePlayer = counterPlayer+1;
		_playerStates[activePlayer] = PlayerState_Manager_Script.PlayerState.FIGHT_DISABLED;
		_inactivePlayer = activePlayer+1;
		
		//Deactive former active player's ships
		foreach(GameObject ship in _activeShips[activePlayer]){
			if(ship){
				ship.GetComponent<ShipStateManager_Script>()._currentState = ShipStateManager_Script.FIGHT_DISABLED;
				_inactiveShips[activePlayer].Add(ship);
			}
		}
		_activeShips[activePlayer].Clear();
		
		
		//Activate former inactive player's ships
		foreach(GameObject ship in _inactiveShips[counterPlayer]){
			if(ship){
				ship.GetComponent<ShipStateManager_Script>()._currentState = ShipStateManager_Script.FIGHT_ENABLED;
				_activeShips[counterPlayer].Add(ship);
			}
		}
		_inactiveShips[counterPlayer].Clear();
		
	}
	
	public void fadeInHealthbars(){
		if(_currentHealthbarAlpha < 1.0f){
			_currentHealthbarAlpha = Fader_Script.fadeIn(_currentHealthbarAlpha, 1.0f);
//			print("fading hbs in: " + _currentHealthbarAlpha);
			changeHealthbarAlphas();
		}
	}
	
	public void fadeOutHealthbars(){
		if(_currentHealthbarAlpha > 0.0f){
			_currentHealthbarAlpha = Fader_Script.fadeOut(_currentHealthbarAlpha, 0.0f);
//			print("fading hbs out: " + _currentHealthbarAlpha);
			changeHealthbarAlphas();
		}
	}
	
	public void changeHealthbarAlphas(){
		for(int i = 0 ; i < _playerStates.Length ; i++){
			foreach(GameObject ship in _inactiveShips[i]){
				if(ship){
					Color alphaCol = ship.transform.FindChild("Healthbar2/current").renderer.material.color;
					alphaCol.a = _currentHealthbarAlpha;
					ship.transform.FindChild("Healthbar2/current").renderer.material.color = alphaCol;
					ship.transform.FindChild("Healthbar2/base").renderer.material.color = alphaCol;
					ship.transform.FindChild("Healthbar2/background").renderer.material.color = alphaCol;
				}
			}
			foreach(GameObject ship in _activeShips[i]){
				if(ship){
					Color alphaCol = ship.transform.FindChild("Healthbar2/current").renderer.material.color;
					alphaCol.a = _currentHealthbarAlpha;
					ship.transform.FindChild("Healthbar2/current").renderer.material.color = alphaCol;
					ship.transform.FindChild("Healthbar2/base").renderer.material.color = alphaCol;
					ship.transform.FindChild("Healthbar2/background").renderer.material.color = alphaCol;
				}
			}
		}
	}
	
}
