using UnityEngine;
using System.Collections;

public class Billboard_Script : MonoBehaviour
{
    private Camera      _Camera;
    public bool         _cameraChanged = true;

	void Start()
	{
		_Camera = Camera.main;
	}
	
    void Update()
    {
        //for Performance, just change the transform, when camera did change
        if (_cameraChanged)
        {
            transform.LookAt(transform.position + _Camera.transform.rotation * Vector3.back, _Camera.transform.rotation * Vector3.up);
            _cameraChanged = false;
        }
    }
}
