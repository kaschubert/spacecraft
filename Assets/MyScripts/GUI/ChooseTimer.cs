using UnityEngine;
using System.Collections;

public class ChooseTimer : MonoBehaviour {
	
	private float time = 10;
	private int roundedTime = 10;
	private bool exceeded = false;
	private bool stop = false;
	
	// Use this for initialization
	void Start () {
		stopTimer();
	}
	
	// Update is called once per frame
	void Update () {
		if(roundedTime == 0)
			exceeded = true;
		
		if(!exceeded && !stop){
			time = time - Time.deltaTime;
			roundedTime = (int)time;
		}
		
	}
	
	public void reset(){
		time = 10;
		roundedTime = 10;
		exceeded = false;
	}
	
	public int getTime(){
		return roundedTime;	
	}
	
	public void stopTimer(){
		stop = true;
		reset();
	}
	
	public void startTimer(){
		stop = false;
	}
	
	public bool isExceeded(){
		return exceeded;
	}
}
