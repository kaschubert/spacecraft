using UnityEngine;
using System.Collections;

public class HUD_Phase1_Script : MonoBehaviour {
	//General
	public Vector3 _touchPoint;
	public int _player;
	private PlayerData_Script _playerDataScript;
	private int _currentScreenHeight;
	public GameObject _gameLogic;
	private PlayerState_Manager_Script _stateScript;

	//Background of HUD
	public int _bgSegmentHeight;
	public int _bgSegmentWidth;
	public float _bgSegmentX;
	public Texture _bgSegmentTex;
	
	//Toolbar
	public GUIContent[] _tbContents;
	public GUIStyle _tbStyle;
	public float _tbWidthPercent; 
	public float _tbX;
	public int _tbHeight;
	public int _tbSelected;
	private Rect _tbRect; 
	private Rect[] _tbPositions;
	
	//CreditBox
	public GUIStyle _cbStyle;
	public float _cbWidthPercent;
	public float _cbHeight;
	public float _cbMargin;
	private Rect _cbRect;
	private Rect _cbRectRotated;
	private float _cbX;
	private float _cbPivotX;
	
	//ConfirmButton
	public GUIStyle _okbStyle;
	public float _okbWidthPercent;
	public float _okbHeight;
	public float _okbMargin;
	private Rect _okbRect;
	private Rect _okbRectRotated;
	private float _okbX;
	private float _okbPivotX;
	
	//DeploymentArea
	public GUIStyle _daStyle;
	public float _daWidthPercent;
	public float _daOffsetX = 0f;
	private int _daCurrentCosts;
	public GameObject _hunterPrototype;
	public GameObject _gunshipPrototype;
	public GameObject _cruiserPrototype;
	public RaycastHit _hit;
	private GameObject _mouseColliderPlane;
	private Rect _daRect;
	private Rect _daRectRotated;
	private const int _OFFSET_X = 150;
	private bool canBuy = false;
	
	//OptionButton
	public GUIStyle _obStyle;
	public Rect _obRect;
	private bool _obToggle;
	private Rect _obTouchRect;
	public bool _isOpaque = false;
	private float _obAlpha = 0f;
//	private float _fadeDeltaTime = 0.0f;
	public bool _fadeIn = false;
	public bool _fadeOut = false;
	
	//OptionChoices
	public Texture _ocSegmentTex;
	public Texture _ocUpperEndTex;
	public GUIStyle _ocStyle;
	public string[] _ocTexts;
	private Rect _quitButtonRect;
	private Rect _quitButtonRectRotated;
	
	//DeployentAreaCoverValue
	private const int _daCv = 10;
	
	//Active Layouts
	private Texture2D _activeBgSeg;
	private Texture2D _activeNormalButton;
	private Texture2D _activeActiveButton;
	private Texture2D _activeEndTex;
	private Texture2D _activeOptionButton;
	
	//Inactive Layouts
	public Texture2D _inactiveBgSeg;
	public Texture2D _inactiveNormalButton;
	public Texture2D _inactiveActiveButton;
	public Texture2D _inactiveEndTex;
	public Texture2D _inactiveOptionButton;
	
	//Statusbar
	public Texture2D _statusBar;
	public float _statusBarX;
		
	public static int ONLY_OK = 0;
	public static int ALL_ELEMENTS = 1;
	
	private ChooseTimer _chooseTimer;
	public GUIStyle _labelStyleTimer;
	public GUIStyle _labelStyleWinLose;
	
	private bool _acceptInput = true;
	
	// Use this for initialization
	void Start () {
		
		//Layouts
		_activeBgSeg = (Texture2D)_bgSegmentTex;
		_activeNormalButton = (Texture2D)_okbStyle.normal.background;
		_activeActiveButton = (Texture2D)_okbStyle.active.background;	
		_activeEndTex = (Texture2D)_ocUpperEndTex;
		_activeOptionButton = (Texture2D)_obStyle.normal.background;
		
		//General
		_currentScreenHeight = 0;
		_playerDataScript = GameObject.Find("HUD_P" + _player).GetComponent<PlayerData_Script>();
		_stateScript = _gameLogic.GetComponent<PlayerState_Manager_Script>();
		_hit = new RaycastHit();
		
		//Background
		_bgSegmentX = 0;
		
		//Toolbar
		_tbPositions = new Rect[_tbContents.Length];
						
		//DeploymentArea
		_mouseColliderPlane = GameObject.Find("MouseColliderPlane");
		//_hunterPrototype = GameObject.Find("Hunter");
		//_gunshipPrototype = GameObject.Find("Gunship_lowPoly");
		
		//OptionButton
		_obToggle = false;
		_obTouchRect = _obRect;	
		_isOpaque = false;
		
		//StatusBar
		_statusBarX = 0;
		
		//Timer
		_chooseTimer = gameObject.GetComponent<ChooseTimer>();
	}
	
	void Update(){
		
	}
	
	//Draw the GUI
	//CAUTION:
	//Every GUI Element gets rotated first by 90 degrees
	//and afterwards by 180 degrees if it belongs to player 1.
	//The positioning in the code happens AFTER the code for the rotation,
	//But has to be understood as if no rotation has been applied yet.
	//However, at runtime, the rotation is executed first.
	//After the code for the positioning, the GUI is rotated back to normal.
	
	//Putting all methods between one single rotation results in ODD effects.
	public void drawGUI () {
		drawDeploymentArea();
		drawStatusBar();
		drawHUD_BG();
		drawToolbar();
		drawCreditBox();
		drawOkButton();
		drawOptionButton();
		drawTimer();
		
		//Adjust to current resolution based on height of screen
		if(Screen.height != _currentScreenHeight){
			_currentScreenHeight = Screen.height;
		}
	}
	
	private void drawStatusBar(){
		//Rotate entire GUI by 90° + 180° if Player 1
		GUIUtility.RotateAroundPivot(90, new Vector2(_statusBarX,0));
		if(_player == 1)
			GUIUtility.RotateAroundPivot(180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		
		//Distance of first tilable background segment from left screen border (BEFORE rotation)
		_statusBarX = Screen.width * _daWidthPercent + _bgSegmentHeight-_daCv + _statusBar.height;
		
		//Create small boxes to put texture-tiles in
		int segments = 0;
		while(Screen.height-(segments*_statusBar.width) > 0){
			Rect segmentRect = new Rect(
				_statusBarX + segments*_statusBar.width,
				0,
				_statusBar.height,
				_statusBar.width);
			
			//Draw a tile
			GUI.DrawTexture(segmentRect,_statusBar);
			
			segments++;
		}
		
		//Rotate entire GUI back to normal (changes above are already effective)
		if(_player == 1)
			GUIUtility.RotateAroundPivot(-180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		GUIUtility.RotateAroundPivot(-90, new Vector2(_statusBarX,0));
	}
	
	private void drawHUD_BG(){
		//Rotate entire GUI by 90° + 180° if Player 1
		GUIUtility.RotateAroundPivot(90, new Vector2(_bgSegmentX,0));
		if(_player == 1)
			GUIUtility.RotateAroundPivot(180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		
		//Distance of first tilable background segment from left screen border (BEFORE rotation)
		_bgSegmentX = Screen.width * _daWidthPercent + _bgSegmentHeight-_daCv;
		
		//Create small boxes to put texture-tiles in
		int segments = 0;
		while(Screen.height-(segments*_bgSegmentWidth) > 0){
			Rect segmentRect = new Rect(
				_bgSegmentX + segments*_bgSegmentWidth,
				0,
				_bgSegmentHeight,
				_bgSegmentWidth);
			
			//Draw a tile
			GUI.DrawTexture(segmentRect,_bgSegmentTex);
			
			segments++;
		}
		
		//Rotate entire GUI back to normal (changes above are already effective)
		if(_player == 1)
			GUIUtility.RotateAroundPivot(-180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		GUIUtility.RotateAroundPivot(-90, new Vector2(_bgSegmentX,0));
	}
	
	private void drawToolbar(){
		
		//Rotate entire GUI by 90° + 180° if Player 1
		GUIUtility.RotateAroundPivot(90, new Vector2(_tbX,0));
		if(_player == 1)
			GUIUtility.RotateAroundPivot(180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		
		//Distance of toolbar from left screen border (BEFORE rotation)
		_tbX = Screen.width * _daWidthPercent - _daCv;
		
		//create toolbar
		_tbRect = new Rect(_tbX,-_tbHeight - _bgSegmentHeight* 0.25f,Screen.height * _tbWidthPercent,_tbHeight);
		GUI.Toolbar(_tbRect, _tbSelected, _tbContents, _tbStyle);
		
		//set _positions to current Rects of toolbar buttons BEFORE ROTATION
		float space = _tbRect.width/_tbPositions.Length;
		float width = space-_tbStyle.margin.right;
		for(int i = 0 ; i < _tbPositions.Length ; i++){
			float offsetX = space*i + _tbStyle.margin.left;
			_tbPositions[i] = new Rect(_tbX + offsetX, -_bgSegmentHeight* 0.25f, width,_tbHeight);
		}
		
		//set _positions to current Rects of buttons AFTER ROTATION
		for(int i = 0 ; i < _tbPositions.Length ; i++){				
			float x = _tbPositions[i].x - (i*space + _tbStyle.margin.left) + _bgSegmentHeight * 0.25f;
			float y = _tbPositions[i].y + (i*_tbPositions[i].width + _tbStyle.margin.left*i) + _bgSegmentHeight * 0.25f;
			
			//corrections for player 1
			if(_player == 1){
				x = Screen.width-(_daRect.width + 0.25f*_bgSegmentHeight + _tbHeight)+10;	
				y = Screen.height-(y + _tbPositions[i].width);
			}
			
			float w = _tbPositions[i].height;
			float h = _tbPositions[i].width;
			_tbPositions[i] = new Rect(x, y, w, h);
		}
			
		//Rotate entire GUI back to normal (changes above are already effective)
		if(_player == 1)
			GUIUtility.RotateAroundPivot(-180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		GUIUtility.RotateAroundPivot(-90, new Vector2(_tbX,0));
		
	}
	
	private void drawCreditBox(){
	
		//Rotate entire GUI by 90° + 180° if Player 1
		GUIUtility.RotateAroundPivot(90, new Vector2(_cbPivotX, 0));
		if(_player == 1)
			GUIUtility.RotateAroundPivot(180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		
		//Set pivot to rotate CreditBox around
		_cbPivotX = _daWidthPercent * Screen.width;
		
		//Distance of CreditBox from left screen border (BEFORE rotation)
		_cbX = _tbWidthPercent * Screen.height + _daWidthPercent * Screen.width;
		
		//Rect BEFORE ROTATION
		_cbRect = new Rect(_cbX + _cbMargin,-_cbHeight - _bgSegmentHeight * 0.25f + _daCv,Screen.height * _cbWidthPercent - _cbMargin,_cbHeight);
		
		//Rect AFTER ROTATION
		float x = _daWidthPercent*Screen.width + _bgSegmentHeight * 0.25f -_daCv;
		float y = _tbWidthPercent*Screen.height + _cbMargin;
			//Corrections for player 1
			if(_player == 1){
				x = Screen.width-(_daRect.width + 0.25f*_bgSegmentHeight + _tbHeight ) + _daCv;		
				y = Screen.height - (_tbWidthPercent*Screen.height + _cbWidthPercent*Screen.height);
			}
		float w = _cbRect.height;
		float h = _cbRect.width;
		_cbRectRotated = new Rect(x,y,w,h);
			
		//Draw CreditBox
		GUI.Box(_cbRect,"Credits \n" + _playerDataScript._credits, _cbStyle);
		
		//Rotate entire GUI back to normal (changes above are already effective)
		if(_player == 1)
			GUIUtility.RotateAroundPivot(-180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		GUIUtility.RotateAroundPivot(-90, new Vector2(_cbPivotX, 0));
	}
	
	private void drawOkButton(){
	
		//Rotate entire GUI by 90° + 180° if Player 1
		GUIUtility.RotateAroundPivot(90, new Vector2(_okbPivotX, 0));
		if(_player == 1)
			GUIUtility.RotateAroundPivot(180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		
		//Set pivot to rotate OK-Button around
		_okbPivotX = _daWidthPercent * Screen.width;
		
		//Distance of OK-Button from left screen border (BEFORE rotation)
		_okbX = _tbWidthPercent * Screen.height + _daWidthPercent * Screen.width + _cbWidthPercent * Screen.height;
		
		//Rect BEFORE ROTATION
		_okbRect = new Rect(_okbX + _okbMargin,-_okbHeight - _bgSegmentHeight * 0.25f +_daCv,Screen.height * _okbWidthPercent - _okbMargin,_okbHeight);
		
		//Rect AFTER ROTATION
		float x = _daWidthPercent*Screen.width + _bgSegmentHeight * 0.25f -_daCv;
		float y = _tbWidthPercent*Screen.height	+ _cbWidthPercent * Screen.height + _okbMargin;
			//Corrections for player 1
			if(_player == 1){
				x = Screen.width-(_daRect.width + 0.25f*_bgSegmentHeight + _tbHeight) +_daCv;
				y = Screen.height - (_tbWidthPercent*Screen.height 
					+ _cbWidthPercent*Screen.height + _okbWidthPercent * Screen.height);
			}
		float w = _okbRect.height;
		float h = _okbRect.width;
		_okbRectRotated = new Rect(x,y,w,h);
		
		//Draw OK-Button
		GUI.Button(_okbRect,"OK", _okbStyle);
		
		//Rotate entire GUI back to normal (changes above are already effective)
		if(_player == 1)
			GUIUtility.RotateAroundPivot(-180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		GUIUtility.RotateAroundPivot(-90, new Vector2(_okbPivotX, 0));		
	}
	
	private void drawDeploymentArea(){
		
		//Rotate entire GUI by 180° if Player 1
		if(_player == 1)
			GUIUtility.RotateAroundPivot(180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
	
		//Rect BEFORE "ROTATION"
		_daRect = new Rect(0,0,Screen.width*_daWidthPercent,Screen.height);
		_daRectRotated = _daRect;
		if(_player == 1){
			_daRectRotated = new Rect(Screen.width-_daRect.width,0,_daRect.width,_daRect.height);
		}
		
		fade();
		
		GUI.Box(_daRect,"", _daStyle);
		
		//Nullify fade by using alpha 1
		//If omitted, entire GUI of other player will fade
		GUI.color = new Color(GUI.color.r,GUI.color.g,GUI.color.b, 1);
		
		//Rotate entire GUI back to normal (changes above are already effective)
		if(_player == 1)
			GUIUtility.RotateAroundPivot(-180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		
	}
	
	private void drawOptionButton(){
		
		//Rotate entire GUI by 180° if Player 1
		if(_player == 1){
			GUIUtility.RotateAroundPivot(180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
			_obTouchRect.x = Screen.width - _obTouchRect.width;
			_obTouchRect.y = Screen.height - _obTouchRect.height;
		}
			
		//Let alpha value go from 0 to 1
		if(_fadeIn){
			if(!_isOpaque){
				_obAlpha = Fader_Script.fadeIn(0.0f, 1.0f);
				if(_obAlpha >= 1.0f){
					_isOpaque = true;
				}
			}
		}
		
		//Let alpha value go from 1 to 0
		if(_fadeOut){			
			if(_isOpaque){
				_obAlpha = Fader_Script.fadeOut(1.0f, 0.0f);
				if(_obAlpha <= 0.0f){
					_isOpaque = false;
				}
			}
		}
		
		//Build OptionButton
		GUI.color = new Color(GUI.color.r,GUI.color.g,GUI.color.b, _obAlpha);
		GUI.Toggle(_obRect, _obToggle, "", _obStyle);
		
		_acceptInput = _obToggle;
		
		//Draw OptionChoices
		if(_obToggle){
			
			//Build Segments and OptionChoices
			Rect segmentRect = new Rect(0,0, _obRect.width, _obRect.height); 
			
			//Build EndSegment with Quit-button
			segmentRect.x += _ocSegmentTex.width;
			GUI.DrawTexture(segmentRect, _ocUpperEndTex);
			
			_quitButtonRect = new Rect(
					segmentRect.x+segmentRect.width*0.25f,
					segmentRect.y+segmentRect.height*0.25f,
					segmentRect.width*0.5f,
					segmentRect.height*0.5f);
			_quitButtonRectRotated = _quitButtonRect;
			if(_player == 1){
				_quitButtonRectRotated = new Rect(
					Screen.width-(segmentRect.x+segmentRect.width*0.25f+_quitButtonRect.width),
					Screen.height-(segmentRect.y+segmentRect.height*0.25f+_quitButtonRect.height),
					segmentRect.width*0.5f,
					segmentRect.height*0.5f
				);
			}
			
			//Rotate Button
			if(_player == 1){
				GUIUtility.RotateAroundPivot(90, new Vector2(
					Screen.width - (_quitButtonRect.x + _quitButtonRect.width*0.5f),
					Screen.height - (_quitButtonRect.y + _quitButtonRect.height*0.5f)));
			} else {
				GUIUtility.RotateAroundPivot(90, new Vector2(
					_quitButtonRect.x + _quitButtonRect.width*0.5f,_quitButtonRect.y + _quitButtonRect.height*0.5f));
			}
			
			GUI.Button(_quitButtonRect, _ocTexts[_ocTexts.Length-1], _ocStyle);
			
			//Rotate back to normal
			if(_player == 1){
				GUIUtility.RotateAroundPivot(-90, new Vector2(
					Screen.width - (_quitButtonRect.x + _quitButtonRect.width*0.5f),
					Screen.height - (_quitButtonRect.y + _quitButtonRect.height*0.5f)));
			} else {
				GUIUtility.RotateAroundPivot(-90, new Vector2(
					_quitButtonRect.x + _quitButtonRect.width*0.5f,_quitButtonRect.y + _quitButtonRect.height*0.5f));
			}
		}
		
		GUI.color = new Color(GUI.color.r,GUI.color.g,GUI.color.b, 1);
		
		//Rotate entire GUI back to normal (changes above are already effective)
		if(_player == 1)
			GUIUtility.RotateAroundPivot(-180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		
		
	}
	
	private void drawTimer(){
		
		//Rotate entire GUI by 90° + 180° if Player 1
		GUIUtility.RotateAroundPivot(90, new Vector2(_statusBarX-_statusBar.height*0.6f, 0));
		if(_player == 1)
			GUIUtility.RotateAroundPivot(180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		
		int roundedTime = _chooseTimer.getTime();
		GUI.Label(new Rect(_statusBarX,0,100,100), "Time: " + roundedTime.ToString() + " sec", _labelStyleTimer);	
		
		if(_chooseTimer.isExceeded()){
			_chooseTimer.reset();	
			
			//randomly choose a point in space on the DeploymentArea
			RaycastHit randomRay;
			int randomX = Random.Range((int)_daRectRotated.xMin, (int)_daRectRotated.xMax);
			int randomY = Random.Range((int)_daRectRotated.yMin, (int)_daRectRotated.yMax);
			
			Ray ray = Camera.main.ScreenPointToRay(new Vector2(randomX, randomY));
			Physics.Raycast(ray, out randomRay);
			_hit.point = randomRay.point;
						
			//Try to buy a ship
			//If credits are not 0 and it still cannot be bought, take the smallest ship
			if(_playerDataScript._credits == 0){
				confirm();
			} else {
				if(!buy()){
					_tbSelected = 0;
					_playerDataScript._selected = 0;
					buy();	
				}
			}
			
			_chooseTimer.stopTimer();
		}
		
		//Rotate entire GUI back to normal (changes above are already effective)
		if(_player == 1)
			GUIUtility.RotateAroundPivot(-180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		GUIUtility.RotateAroundPivot(-90, new Vector2(_statusBarX-_statusBar.height*0.6f, 0));		
	}
	
	//Let a GUI Element fade in and out (by modifying alpha value of color)
	private void fade(){
		
		//Set Frequency and Amplitude of the transition
		float freq = 0.75f;
		float amp = 0.3f;
		//Avoid long phases of invisbility (meaning alpha <= 0) by inverting the sign
		if((Mathf.Sin(freq * Time.time)*amp < 0)){
			freq *= -1;
		}
		
		//Change alpha value according to time
		float alpha = Mathf.Lerp(0.0f,1.0f,Mathf.Sin(freq * Time.time)*amp);
		
		
		
		//Apply alpha value to entire GUI
		GUI.color = new Color(GUI.color.r,GUI.color.g,GUI.color.b, alpha);
	}
	
//	public void fadeIn(){
//		
//		_fadeDeltaTime += Time.deltaTime;
//		
//		if(!_isOpaque){
//			//Set Frequency and Amplitude of the transition
//			float freq = 0.75f;
//			float amp = 1f;
////			//Avoid long phases of invisbility (meaning alpha <= 0) by inverting the sign
////			if((Mathf.Sin(freq * _fadeDeltaTime)*amp < 0)){
////				freq *= -1;
////			}
//			
//			//Change alpha value according to time
//			float alpha = Mathf.Lerp(0.0f,1.0f, _fadeDeltaTime);
//			
////			print(alpha);
//			
//			if(alpha >= amp){
//				_isOpaque = true;
//			}
//			
//			_obAlpha = alpha;
//			
//			//Apply alpha value to entire GUI
//			GUI.color = new Color(GUI.color.r,GUI.color.g,GUI.color.b, alpha);
//		} else {
//			_fadeDeltaTime = 0.0f;	
//		}
//	}
	
//	public void fadeOut(){
//		
//		_fadeDeltaTime += Time.deltaTime;
//		
//		if(_isOpaque){
//			//Set Frequency and Amplitude of the transition
//			float freq = 0.75f;
//			float amp = 1f;
////			//Avoid long phases of invisbility (meaning alpha <= 0) by inverting the sign
////			if((Mathf.Sin(freq * _fadeDeltaTime)*amp < 0)){
////				freq *= -1;
////			}
//			
//			//Change alpha value according to time
//			float alpha = Mathf.Lerp(1.0f,0.0f,_fadeDeltaTime);
//			
//			print(alpha);
//			
//			if(alpha <= 0){
//				_isOpaque = false;
////				_fadeOut = false;
//			}
//			
//			_obAlpha = alpha;
//			
//			//Apply alpha value to entire GUI
//			GUI.color = new Color(GUI.color.r,GUI.color.g,GUI.color.b, alpha);
//		} else {
//			_fadeDeltaTime = 0.0f;	
//		}
//	}
	
	public void setGuiActivity(bool isActive, int elements){
		if(elements == ALL_ELEMENTS){	
			if(isActive){
				canBuy = true;
				_bgSegmentTex = _activeBgSeg;
				_okbStyle.normal.background = _activeNormalButton;
				_okbStyle.active.background = _activeActiveButton;
				_ocUpperEndTex = _activeEndTex;
				_ocStyle.normal.background = _activeNormalButton;
				_ocStyle.active.background = _activeActiveButton;
				_tbStyle.normal.background = _activeNormalButton;
				_tbStyle.onNormal.background = _activeActiveButton;
				_cbStyle.normal.background = _activeNormalButton;
				_cbStyle.active.background = _activeActiveButton;
				_obStyle.normal.background = _activeOptionButton;
			} else {
				canBuy = false;
				_bgSegmentTex = _inactiveBgSeg;
				_okbStyle.normal.background = _inactiveNormalButton;
				_okbStyle.active.background = _inactiveActiveButton;
				_ocUpperEndTex = _inactiveEndTex;
				_ocStyle.normal.background = _inactiveNormalButton;
				_ocStyle.active.background = _inactiveActiveButton;
				_tbStyle.normal.background = _inactiveNormalButton;
				_tbStyle.onNormal.background = _inactiveActiveButton;
				_cbStyle.normal.background = _inactiveNormalButton;
				_cbStyle.active.background = _inactiveActiveButton;
				_obStyle.normal.background = _inactiveOptionButton;					
			}
		} else {
			if(isActive){
				_okbStyle.normal.background = _activeNormalButton;
				_okbStyle.active.background = _activeActiveButton;
			} else {
				_okbStyle.normal.background = _inactiveNormalButton;
				_okbStyle.active.background = _inactiveActiveButton;
			}
		}
	}
	
//Create a delegate for the toolbar
//	delegate int Toolbar();
//	Toolbar MakeToolbar(Rect rect, GUIContent[] contents, int selected, GUIStyle style)
//	{
//		Toolbar toolbar = delegate()
//		{
//			selected = _tbSelected;
//			selected = GUI.Toolbar(rect, selected, contents, style);
//			return selected;
//		};
//		return toolbar;
//	}
	
	//Iterate through all Buttons of the toolbar
	//and check if the point in the Vector
	//matches their positions
	public bool checkTouch(Vector3 touchPoint){
		bool isTouched = false;
			
		if(_acceptInput){
			//Check if Quit-Button is hit
			if(_quitButtonRectRotated.Contains(touchPoint) && !isTouched){
				
				print("Shutting down...");
				Application.Quit();
				isTouched = true;
			}
		}
			
		//Check if toolbar is hit
		for(int i = 0 ; i < _tbPositions.Length ; i++){
			if(_tbPositions[i].Contains(touchPoint) && !isTouched){
				print("Hit ToolbarButton " + i + "!");
				_tbSelected = i;
				_playerDataScript._selected = _tbSelected;
				isTouched = true;
				
//				_chooseTimer.startTimer();
				_hit.point = new Vector3();
			}
		}
		
		//Check if CreditBox is hit
		if(_cbRectRotated.Contains(touchPoint) && !isTouched){
			print("Hit CreditBox!");
			isTouched = true;
		}
		
		//Check if OK-Button is hit
		if(_okbRectRotated.Contains(touchPoint) && !isTouched){
			print("hit OK!");
			confirm();
			isTouched = true;
		}
				
		//Check Option Button
		if(_obTouchRect.Contains(touchPoint) && !isTouched 
				&& _isOpaque && (_obStyle.normal.background != _inactiveOptionButton)){
			print("Options Button touched!");
			
			//Toggle Button Selection on Touch
			_obToggle = !_obToggle;	
			
			isTouched = true;
		}
		
		//Check if Deploymentarea is hit
		if(_daRectRotated.Contains(touchPoint) && !isTouched && canBuy){
			Debug.Log("You hit the Deploymentarea!");
			
			//Revert the positional change made above (Raycast needs Input coords)
			touchPoint.y = Screen.height-touchPoint.y;

			//Check what the RayCast hits
			Ray ray = Camera.main.ScreenPointToRay(touchPoint);
			if (Physics.Raycast(ray, out _hit)){
				Debug.DrawRay(Input.mousePosition,_hit.point,Color.cyan);
				print("You hit " + _hit.collider.gameObject.name);
				
				//If MouseColliderPlane is hit (instead of, for instance, a ship)
				//buy a ship
				if (_mouseColliderPlane.collider == _hit.collider){
					Debug.Log("You hit the MouseColliderPlane!");
					buy();
				} else {
					//hit.collider.gameObject.GetComponent<ShipControl_Script>()._shipState = ShipControl_Script.ShipState.BuySold;
					//TODO wenns nicht funktioniert, einfach hier verkaufen
				}
			}

			isTouched = true;
		}
			
		return isTouched;
	}
	
	private bool buy(){
		//Set starting and end position for the ship flying in
		Vector3 startAt = new Vector3(_hit.point.x + _daOffsetX, _hit.point.y, _hit.point.z);
		_chooseTimer.reset();
		//Vector3 moveTo = new Vector3(_hit.point.x, _hit.point.y, _hit.point.z);
		
		
		switch(_playerDataScript._selected){
		case 0:
			if(_playerDataScript._credits - _hunterPrototype.GetComponent<Attributes_Script>()._costs < 0){
				return false;
			}
			break;
		case 1:
			if(_playerDataScript._credits - _gunshipPrototype.GetComponent<Attributes_Script>()._costs < 0){
				return false;
			}
			break;
		case 2:
			if(_playerDataScript._credits - _cruiserPrototype.GetComponent<Attributes_Script>()._costs < 0){
				return false;
			}			
			break;
		}
		
		//Instantiate correct ship by type
		switch(_playerDataScript._selected){
		case 0:
			_playerDataScript._credits -= _hunterPrototype.GetComponent<Attributes_Script>()._costs;
			GameObject hunterClone = (GameObject)Instantiate(_hunterPrototype, startAt, _hunterPrototype.transform.rotation);
			hunterClone.transform.FindChild("_Attack_Marker").gameObject.SetActiveRecursively(false);
			hunterClone.GetComponent<Attributes_Script>()._player = _player;
			hunterClone.GetComponent<Attributes_Script>()._instantiationCoord = startAt;
			hunterClone.GetComponent<ShipControl_Script>()._destination = _hit.point;
			hunterClone.GetComponent<ShipControl_Script>()._teamColor = _playerDataScript._teamColor;
			break;
		case 1:
			_playerDataScript._credits -= _gunshipPrototype.GetComponent<Attributes_Script>()._costs;
			GameObject gunshipClone = (GameObject)Instantiate(_gunshipPrototype, startAt, _gunshipPrototype.transform.rotation);
			gunshipClone.transform.FindChild("_Attack_Marker").gameObject.SetActiveRecursively(false);
			gunshipClone.GetComponent<Attributes_Script>()._player = _player;		
			gunshipClone.GetComponent<Attributes_Script>()._instantiationCoord = startAt;
			gunshipClone.GetComponent<ShipControl_Script>()._destination = _hit.point;
			gunshipClone.GetComponent<ShipControl_Script>()._teamColor = _playerDataScript._teamColor;
			break;
		case 2:
			_playerDataScript._credits -= _cruiserPrototype.GetComponent<Attributes_Script>()._costs;
			GameObject cruiserClone = (GameObject)Instantiate(_cruiserPrototype, startAt, _cruiserPrototype.transform.rotation);
			cruiserClone.transform.FindChild("_Attack_Marker").gameObject.SetActiveRecursively(false);
			cruiserClone.GetComponent<Attributes_Script>()._player = _player;
			cruiserClone.GetComponent<Attributes_Script>()._instantiationCoord = startAt;
			cruiserClone.GetComponent<ShipControl_Script>()._destination = _hit.point;
			cruiserClone.GetComponent<ShipControl_Script>()._teamColor = _playerDataScript._teamColor;
			break;
		}
		
		return true;
	}
	
	//Set player's state to LockedIn, if this player has spent all credits
	private void confirm(){
		if(_playerDataScript._credits > 0){
			print("You have to spend all your credits first!");	
		} else if(_playerDataScript._credits == 0){
			_acceptInput = false;
			print("Player " + _player + " is ready!");
			_stateScript._playerStates[_player-1] = PlayerState_Manager_Script.PlayerState.BUY_LOCKED_IN;
			
			_obToggle = false;
			_obStyle.normal.background = _inactiveOptionButton;
			_obStyle.active.background = _inactiveOptionButton;
			_obTouchRect = new Rect(0,0,0,0);
			_chooseTimer.stopTimer();
		}
	}
		
}
