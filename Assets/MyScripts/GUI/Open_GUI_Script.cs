using UnityEngine;
using System.Collections;

public class Open_GUI_Script : BBTouchableObject {
	
	private bool touched = false;
	private Vector3 x1;
	private Vector3 x2;
	public GUI_Script _guiScript;
	private float deltaTime = 0f;
	private HUD_Phase1_Script _player1_hudP1_Script;
	private HUD_Phase1_Script _player2_hudP1_Script;
	private HUD_Phase2_Script _player1_hudP2_Script;
	private HUD_Phase2_Script _player2_hudP2_Script;
	
	public GameObject _gameLogic;
	private PlayerState_Manager_Script _stateScript;
	
	// Use this for initialization
	void Start () {
		GameObject hud1 = GameObject.Find("HUD_P1");
		_player1_hudP1_Script = hud1.GetComponent<HUD_Phase1_Script>();
		_player1_hudP2_Script = hud1.GetComponent<HUD_Phase2_Script>();
		GameObject hud2 = GameObject.Find("HUD_P2");
		_player2_hudP1_Script = hud2.GetComponent<HUD_Phase1_Script>();
		_player2_hudP2_Script = hud2.GetComponent<HUD_Phase2_Script>();
		
		_stateScript = _gameLogic.GetComponent<PlayerState_Manager_Script>();
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public override void handleSingleTouch(BBTouchEvent aTouch) 
	{
		if(!touched){	
			x1 = aTouch.lastScreenPosition;
			print("you touched the mouseColliderPlane");
		}
		
		deltaTime += Time.deltaTime;
		x2 = aTouch.lastScreenPosition;
			
		touched = true;
	}
	
	public override void noTouches(){
		
		if(Vector3.Distance(x1, x2) > 100){
			print("Distance: " + Vector3.Distance(x1, x2));
		
		
//		if(deltaTime >= 1.0f){
			print("trying to fade");
			
			//Concerning Player 2
			if(x1.x < Screen.width*0.5){
				
				//if player is in Phase 1
				if(_stateScript._playerStates[1] == PlayerState_Manager_Script.PlayerState.BUY_LOCKED_IN ||
					_stateScript._playerStates[1] == PlayerState_Manager_Script.PlayerState.BUY_NO_CREDITS ||
					_stateScript._playerStates[1] == PlayerState_Manager_Script.PlayerState.BUY_SELECTION){
					if(!_player2_hudP1_Script._isOpaque){
						_player2_hudP1_Script._fadeIn = true;
						_player2_hudP1_Script._fadeOut = false;
					} else {
						_player2_hudP1_Script._fadeIn = false;
						_player2_hudP1_Script._fadeOut = true;
					}
				//if player is in phase 2
				} else {
					if(!_player2_hudP2_Script._isOpaque){
						_player2_hudP2_Script._fadeIn = true;
						_player2_hudP2_Script._fadeOut = false;
					} else {
						_player2_hudP2_Script._fadeIn = false;
						_player2_hudP2_Script._fadeOut = true;
					}
				}
				
			//Concerning Player 1
			} else {
				
				//if player is in Phase 1
				if(_stateScript._playerStates[0] == PlayerState_Manager_Script.PlayerState.BUY_LOCKED_IN ||
					_stateScript._playerStates[0] == PlayerState_Manager_Script.PlayerState.BUY_NO_CREDITS ||
					_stateScript._playerStates[0] == PlayerState_Manager_Script.PlayerState.BUY_SELECTION){
					if(!_player1_hudP1_Script._isOpaque){
						_player1_hudP1_Script._fadeIn = true;
						_player1_hudP1_Script._fadeOut = false;
					} else {
						_player1_hudP1_Script._fadeIn = false;
						_player1_hudP1_Script._fadeOut = true;
					}
				//if player is in phase 2
				} else {
					if(!_player1_hudP2_Script._isOpaque){
						_player1_hudP2_Script._fadeIn = true;
						_player1_hudP2_Script._fadeOut = false;
					} else {
						_player1_hudP2_Script._fadeIn = false;
						_player1_hudP2_Script._fadeOut = true;
					}
				}
			}
		}
//		}		
		
		deltaTime = 0f;
		touched = false;
		
		x1 = new Vector3(0, 0, 0);
		x2 = new Vector3(0, 0, 0);
	}
}
