using UnityEngine;
using System.Collections;

public class GUI_Script : MonoBehaviour {
	public int _phase = 1;
	public GUIStyle _startButtonStyle;
	
	public GameObject _gameLogic;
	private PlayerState_Manager_Script _stateScript;
//	public State_Manager_Script.PlayerState _state;
	
	//Players' scripts for phase 1
	private HUD_Phase1_Script[] _playerGuiPhase1;
	
	//Players' scripts for phase 2
	private HUD_Phase2_Script[] _playerGuiPhase2;
	
	
	public bool[] _playerReady;
	
	private Rect _startButtonRect;
	private bool _activateStartButton = true;

	// Use this for initialization
	void Start () {
		
		_stateScript = _gameLogic.GetComponent<PlayerState_Manager_Script>();
		
		//Get players' scripts for phase 1
		_playerGuiPhase1 = new HUD_Phase1_Script[2];
		_playerGuiPhase1[0] = GameObject.Find("HUD_P1").GetComponent<HUD_Phase1_Script>();
		_playerGuiPhase1[1] = GameObject.Find("HUD_P2").GetComponent<HUD_Phase1_Script>();
		
		//Get players' scripts for phase 2
		_playerGuiPhase2 = new HUD_Phase2_Script[2];
		_playerGuiPhase2[0] = GameObject.Find("HUD_P1").GetComponent<HUD_Phase2_Script>();
		_playerGuiPhase2[1] = GameObject.Find("HUD_P2").GetComponent<HUD_Phase2_Script>();
		
		_playerReady = new bool[2];
		
		_startButtonRect = new Rect(
			Screen.width*0.5f - _startButtonStyle.normal.background.width*0.5f, 
			Screen.height*0.5f - _startButtonStyle.normal.background.height*0.5f,
			_startButtonStyle.normal.background.width,
			_startButtonStyle.normal.background.height);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnGUI(){
		
		for(int i = 0 ; i < _stateScript._playerStates.Length ; i++){
			switch(_stateScript._playerStates[i]){
			case PlayerState_Manager_Script.PlayerState.BUY_SELECTION:
				
				
				_playerGuiPhase1[i].drawGUI();
				break;
			case PlayerState_Manager_Script.PlayerState.BUY_NO_CREDITS:
				
				
				_playerGuiPhase1[i].drawGUI();
				break;
			case PlayerState_Manager_Script.PlayerState.BUY_LOCKED_IN:
				
				//Idee: einfach die farbwerte der einzelnen/alle GUI-Elemente ändern --> leich viel R, G und B gibt grau.
				
				_playerGuiPhase1[i].drawGUI();
				break;
			case PlayerState_Manager_Script.PlayerState.FIGHT_ENABLED:
				_playerGuiPhase2[i].drawGUI();
				break;
			case PlayerState_Manager_Script.PlayerState.FIGHT_DISABLED:
				_playerGuiPhase2[i].drawGUI();
				break;
			case PlayerState_Manager_Script.PlayerState.FIGHT_WARP:
				_playerGuiPhase2[i].drawGUI();
				break;
			case PlayerState_Manager_Script.PlayerState.WIN:
				_playerGuiPhase2[i].drawGUI();
				break;
			case PlayerState_Manager_Script.PlayerState.LOSE:
				_playerGuiPhase2[i].drawGUI();
				break;
			}
		}
		//TODO weiterbasteln, beißt sich mit anderen ausgrau-aktionen!
//		if(_stateScript._gameStarted){
//			_playerGuiPhase1[0].setGuiActivity(true, HUD_Phase1_Script.ALL_ELEMENTS);
//			_playerGuiPhase1[1].setGuiActivity(true, HUD_Phase1_Script.ALL_ELEMENTS);
//		} else {
//			_playerGuiPhase1[0].setGuiActivity(false, HUD_Phase1_Script.ALL_ELEMENTS);
//			_playerGuiPhase1[1].setGuiActivity(false, HUD_Phase1_Script.ALL_ELEMENTS);
//		}
		
		drawStartButton(_activateStartButton);
		
	}
	
	//Check if the player GUIs are touched
	public bool touchCheckGUI(Vector3 touchPoint){
		bool touched = false;
		
		//The mouseclick's y starts with 0 at the bottom of the screen,
		//whereas the Rects' y are drawn with 0 being at the top of the screen
		touchPoint.y = Screen.height-touchPoint.y;
//		print("touched");
		
		//Check Start Button
		if(_startButtonRect.Contains(touchPoint) && !touched){
			print("Start Button touched!");
			
			_activateStartButton = false;
			_stateScript._gameStarted = true;
			
			touched = true;
		}
		
		
		
		for(int i = 0 ; i < _stateScript._playerStates.Length ; i++){
			switch(_stateScript._playerStates[i]){
			case PlayerState_Manager_Script.PlayerState.BUY_SELECTION:
			case PlayerState_Manager_Script.PlayerState.BUY_NO_CREDITS:
			case PlayerState_Manager_Script.PlayerState.BUY_LOCKED_IN:{
				
				touched = _playerGuiPhase1[i].checkTouch(touchPoint);
				
				break;
				}
			case PlayerState_Manager_Script.PlayerState.FIGHT_ENABLED:
			case PlayerState_Manager_Script.PlayerState.FIGHT_DISABLED:
			case PlayerState_Manager_Script.PlayerState.FIGHT_WARP:
			case PlayerState_Manager_Script.PlayerState.WIN:
			case PlayerState_Manager_Script.PlayerState.LOSE:{
				touched = _playerGuiPhase2[i].checkTouch(touchPoint);
				
				break;
				}
			}
			
			//Leave loop if has been touched
			if(touched){
				_stateScript._fadeInHealthbars = false;
				break;
			}
		}		
					
		return touched;
	}
	
	private void drawStartButton(bool active){	
		if(active){
			if(!GUI.Button(_startButtonRect, "START", _startButtonStyle)){
				_playerGuiPhase1[0].setGuiActivity(false, HUD_Phase1_Script.ALL_ELEMENTS);
				_playerGuiPhase1[1].setGuiActivity(false, HUD_Phase1_Script.ALL_ELEMENTS);	
			}
		}
	}
}
