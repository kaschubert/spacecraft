using UnityEngine;
using System.Collections;

public class Full_Billboard_Script : MonoBehaviour
{
    private Camera _Camera;

    void Start()
    {
        _Camera = Camera.main;
    }

    void Update()
    {
        transform.LookAt(transform.position + _Camera.transform.rotation * Vector3.back, _Camera.transform.rotation * Vector3.up);
    }
}
