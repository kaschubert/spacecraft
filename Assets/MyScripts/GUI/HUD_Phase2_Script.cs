using UnityEngine;
using System.Collections;

public class HUD_Phase2_Script : MonoBehaviour {
	
	
	//OptionButton
	public GUIStyle _obStyle;
	public Rect _obRect;
	public int _player;
	private bool _obToggle;
	private Rect _obTouchRect;
	
	//OptionChoices
	public Texture _ocSegmentTex;
	public Texture _ocUpperEndTex;
	public GUIStyle _ocStyle;
	public GUIStyle _ocWarpStyle;
	public string[] _ocTexts;
	private bool _ocWarpToggle;
	private Rect _ocQuitButtonRect;
	private Rect _ocWarpButtonRect;
	private Rect _ocEndTurnButtonRect;
	private Rect _ocQuitTouchRect;
	private Rect _ocWarpTouchRect;
	private Rect _ocEndTurnTouchRect;
	
	public Texture2D _activeOptionButton;
	public Texture2D _inactiveOptionButton;
	private Rect _obTouchRectP1;
	private Rect _obTouchRectP2;	
	
	private PlayerData_Script _playerDataScript;
	public GameObject _gameLogic;
	private PlayerState_Manager_Script _psm;
	public GUIStyle _labelStyleWinLose;
	
	public bool _gameEnded = false;
	public bool _matchWon = false;
	public Texture2D _cleanBeginSegment;
	
	public bool _fadeIn = false;
	public bool _fadeOut = false;
	public bool _isOpaque = true;
	public float _obAlpha = 1.0f;
	private bool _acceptInput = true;

	// Use this for initialization
	void Start () {
		
		//OptionButton
		_ocWarpToggle = false;
		_obToggle = false;
		_obTouchRect = _obRect;
		_obTouchRectP2 = _obRect;
		_obTouchRectP1 = _obRect;
		_obTouchRectP1.x = Screen.width - _obRect.width;
		_obTouchRectP1.y = Screen.height - _obRect.height;
		
		_playerDataScript = this.GetComponent<PlayerData_Script>();
		_psm = _gameLogic.GetComponent<PlayerState_Manager_Script>();
	
	}
	
	void Awake () {
		_psm = _gameLogic.GetComponent<PlayerState_Manager_Script>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void drawGUI(){
		
		if(_playerDataScript._player == _psm._inactivePlayer && !_gameEnded){
			_obToggle = false;
		}
		
		//Rotate entire GUI by 180° if Player 1
		if(_player == 1){
			GUIUtility.RotateAroundPivot(180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
			_obTouchRect.x = Screen.width - _obTouchRect.width;
			_obTouchRect.y = Screen.height - _obTouchRect.height;
			//_obTouchRectP1 = _obTouchRect;
		}
			
		//Let alpha value go from 0 to 1
		if(_fadeIn){
			if(!_isOpaque){
				_obAlpha = Fader_Script.fadeIn(0.0f, 1.0f);
				if(_obAlpha >= 1.0f){
					_isOpaque = true;
				}
			}
		}
		
		//Let alpha value go from 1 to 0
		if(_fadeOut){			
			if(_isOpaque){
				_obAlpha = Fader_Script.fadeOut(1.0f, 0.0f);
				if(_obAlpha <= 0.0f){
					_isOpaque = false;
				}
			}
		}
		
		//Build OptionButton
		GUI.color = new Color(GUI.color.r,GUI.color.g,GUI.color.b, _obAlpha);
		GUI.Toggle(_obRect, _obToggle, "", _obStyle);
		_acceptInput = _obToggle;
				
		if(_obToggle){
			
//			//Build Segments and OptionChoices
			Rect segmentRect = new Rect(0,0, _obRect.width, _obRect.height); 
			
			segmentRect = buildWarpButton(segmentRect);
			segmentRect = buildEndTurnButton(segmentRect);
			segmentRect = buildQuitButton(segmentRect);
			
		}
		
		
		//Rotate entire GUI back to normal (changes above are already effective)
		if(_player == 1)
			GUIUtility.RotateAroundPivot(-180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		
		if(_gameEnded){
			GUI.color = new Color(GUI.color.r,GUI.color.g,GUI.color.b, 1);
			drawEndingScreen(_matchWon);	
		}
		
		GUI.color = new Color(GUI.color.r,GUI.color.g,GUI.color.b, 1);
	}
	
	//Check if touchPoint is on GUI Element
	public bool checkTouch(Vector3 touchPoint){
		bool isTouched = false;
		
		if(_acceptInput){
			//Check if Quit-Button is hit
			if(_ocQuitTouchRect.Contains(touchPoint) && !isTouched){
				print("Shutting down...");
				print("ocQuitRectCoord: " + _ocQuitTouchRect);
				print("TouchPoint: " + touchPoint);
				Application.Quit();
				isTouched = true;
			}
			
			//Check if Warp-Button is hit
			if(_ocWarpTouchRect.Contains(touchPoint) && !isTouched){
				//if(!_ocWarpToggle)
					//print("Warp Mode activated!");
				//else
					//print("Warp Mode deactivated!");
				
				//Toggle Button Selection on Touch
				_ocWarpToggle = !_ocWarpToggle;
				_playerDataScript._warpModeActive = !_playerDataScript._warpModeActive;
				print("Warp Mode toggled to " + _playerDataScript._warpModeActive + " for player " + _playerDataScript._player);
				
				isTouched = true;
			}
			
			//Check if EndTurn-Button is hit
			if(_ocEndTurnTouchRect.Contains(touchPoint) && !isTouched){
				print("EndTurn Button touched!");
				
				_playerDataScript._globalRound++;
				print("Round of player " + _playerDataScript._player + " set to " + _playerDataScript._globalRound);
				
				_psm.forceTurnEnd();
				print("End of Turn forced");
				
				isTouched = true;
			}
		}
		
		//Check Option Button
		if(_obTouchRect.Contains(touchPoint) && !isTouched && _isOpaque){
			print("Options Button touched!");
			
			//Toggle Button Selection on Touch
			_obToggle = !_obToggle;
			_acceptInput = _isOpaque;
		
			isTouched = true;
		}
		
		return isTouched;
	}
	
	private Rect buildWarpButton(Rect segmentRect){
		//Build background for warp-button
		segmentRect.x += _ocSegmentTex.width;
		GUI.DrawTexture(segmentRect, _ocSegmentTex);
		
		_ocWarpButtonRect = new Rect(
				segmentRect.x+segmentRect.width*0.25f,
				segmentRect.y+segmentRect.height*0.25f,
				segmentRect.width*0.5f,
				segmentRect.height*0.5f);
		_ocWarpTouchRect = _ocWarpButtonRect;
		
		//Rotate Button
		if(_player == 1){
			GUIUtility.RotateAroundPivot(90, new Vector2(
				Screen.width - (_ocWarpButtonRect.x + _ocWarpButtonRect.width*0.5f),
				Screen.height - (_ocWarpButtonRect.y + _ocWarpButtonRect.height*0.5f)));
			_ocWarpTouchRect.x = Screen.width - (_ocWarpTouchRect.x + _ocWarpTouchRect.width);
			_ocWarpTouchRect.y = Screen.height - (_ocWarpTouchRect.y + _ocWarpTouchRect.height);
		} else {
			GUIUtility.RotateAroundPivot(90, new Vector2(
				_ocWarpButtonRect.x + _ocWarpButtonRect.width*0.5f,_ocWarpButtonRect.y + _ocWarpButtonRect.height*0.5f));
		}

		//Build actuel Warp-Button
		GUI.Toggle(_ocWarpButtonRect,_ocWarpToggle, _ocTexts[0], _ocWarpStyle);
		
		//Rotate back to normal
		if(_player == 1){
			GUIUtility.RotateAroundPivot(-90, new Vector2(
				Screen.width - (_ocWarpButtonRect.x + _ocWarpButtonRect.width*0.5f),
				Screen.height - (_ocWarpButtonRect.y + _ocWarpButtonRect.height*0.5f)));
		} else {
			GUIUtility.RotateAroundPivot(-90, new Vector2(
				_ocWarpButtonRect.x + _ocWarpButtonRect.width*0.5f,_ocWarpButtonRect.y + _ocWarpButtonRect.height*0.5f));
		}
		
		return segmentRect;
	}
	
	private Rect buildEndTurnButton(Rect segmentRect){
		//Build background for warp-button
		segmentRect.x += _ocSegmentTex.width;
		GUI.DrawTexture(segmentRect, _ocSegmentTex);
		
		_ocEndTurnButtonRect = new Rect(
				segmentRect.x+segmentRect.width*0.25f,
				segmentRect.y+segmentRect.height*0.25f,
				segmentRect.width*0.5f,
				segmentRect.height*0.5f);
		_ocEndTurnTouchRect = _ocEndTurnButtonRect;
		
		//Rotate Button
		if(_player == 1){
			GUIUtility.RotateAroundPivot(90, new Vector2(
				Screen.width - (_ocEndTurnButtonRect.x + _ocEndTurnButtonRect.width*0.5f),
				Screen.height - (_ocEndTurnButtonRect.y + _ocEndTurnButtonRect.height*0.5f)));
			_ocEndTurnTouchRect.x = Screen.width - (_ocEndTurnTouchRect.x + _ocEndTurnTouchRect.width);
			_ocEndTurnTouchRect.y = Screen.height - (_ocEndTurnTouchRect.y + _ocEndTurnTouchRect.height);
		} else {
			GUIUtility.RotateAroundPivot(90, new Vector2(
				_ocEndTurnButtonRect.x + _ocEndTurnButtonRect.width*0.5f,_ocEndTurnButtonRect.y + _ocEndTurnButtonRect.height*0.5f));
		}

		//Build actuel EndTurn-Button
		GUI.Button(_ocEndTurnButtonRect, _ocTexts[1], _ocStyle);
		
		//Rotate back to normal
		if(_player == 1){
			GUIUtility.RotateAroundPivot(-90, new Vector2(
				Screen.width - (_ocEndTurnButtonRect.x + _ocEndTurnButtonRect.width*0.5f),
				Screen.height - (_ocEndTurnButtonRect.y + _ocEndTurnButtonRect.height*0.5f)));
		} else {
			GUIUtility.RotateAroundPivot(-90, new Vector2(
				_ocEndTurnButtonRect.x + _ocEndTurnButtonRect.width*0.5f,_ocEndTurnButtonRect.y + _ocEndTurnButtonRect.height*0.5f));
		}
		
		return segmentRect;
	}
	
	private Rect buildQuitButton(Rect segmentRect){
		//Build EndSegment-background for quit-button
		segmentRect.x += _ocSegmentTex.width;
		GUI.DrawTexture(segmentRect, _ocUpperEndTex);
		
		_ocQuitButtonRect = new Rect(
				segmentRect.x+segmentRect.width*0.25f,
				segmentRect.y+segmentRect.height*0.25f,
				segmentRect.width*0.5f,
				segmentRect.height*0.5f);
		_ocQuitTouchRect = _ocQuitButtonRect;
		
		//Rotate Button
		if(_player == 1){
			GUIUtility.RotateAroundPivot(90, new Vector2(
				Screen.width - (_ocQuitButtonRect.x + _ocQuitButtonRect.width*0.5f),
				Screen.height - (_ocQuitButtonRect.y + _ocQuitButtonRect.height*0.5f)));
			_ocQuitTouchRect.x = Screen.width - (_ocQuitTouchRect.x + _ocQuitTouchRect.width);
			_ocQuitTouchRect.y = Screen.height - (_ocQuitTouchRect.y + _ocQuitTouchRect.height);
		} else {
			GUIUtility.RotateAroundPivot(90, new Vector2(
				_ocQuitButtonRect.x + _ocQuitButtonRect.width*0.5f,_ocQuitButtonRect.y + _ocQuitButtonRect.height*0.5f));
		}
		
		GUI.Button(_ocQuitButtonRect, _ocTexts[_ocTexts.Length-1], _ocStyle);
		
		//Rotate back to normal
		if(_player == 1){
			GUIUtility.RotateAroundPivot(-90, new Vector2(
				Screen.width - (_ocQuitButtonRect.x + _ocQuitButtonRect.width*0.5f),
				Screen.height - (_ocQuitButtonRect.y + _ocQuitButtonRect.height*0.5f)));
		} else {
			GUIUtility.RotateAroundPivot(-90, new Vector2(
				_ocQuitButtonRect.x + _ocQuitButtonRect.width*0.5f,_ocQuitButtonRect.y + _ocQuitButtonRect.height*0.5f));
		}
		
		return segmentRect;
	}
	
	public void drawEndingScreen(bool matchWin){
//		print(matchWin);
		string text = "GEWONNEN";
		
		if(!matchWin){
			text = "VERLOREN";
		}
		
//		print("obrect" + _obRect);
		
		Vector2 pivot = new Vector2(_obRect.width*0.5f, _obRect.height*0.5f);
		float offsetX = Screen.height*0.5f - _obRect.width*1.5f;
		float offsetY = Screen.width*0.25f;
		
//		print("pivot" + pivot);
		
		GUIUtility.RotateAroundPivot(90, pivot);
		if(_player == 1)
			GUIUtility.RotateAroundPivot(180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		
		Rect beginSegmentRect = new Rect(0 + offsetX,-offsetY,_obRect.width, _obRect.height);
		GUI.DrawTexture(beginSegmentRect, _cleanBeginSegment);		
		
		Rect midSegmentRect = new Rect(_obRect.width + offsetX,-offsetY,_obRect.width, _obRect.height);
		GUI.DrawTexture(midSegmentRect, _ocSegmentTex);		
		
		Rect endSegmentRect = new Rect(_obRect.width*2 + offsetX,-offsetY,_obRect.width, _obRect.height);
		GUI.DrawTexture(endSegmentRect, _ocUpperEndTex);	
		
		float labelX = midSegmentRect.x + _obRect.width*0.5f-70;
		float labelY = midSegmentRect.y + _obRect.width*0.5f-10;
		GUI.Label(new Rect(labelX,labelY,200,100), text, _labelStyleWinLose);
		
		if(_player == 1)
			GUIUtility.RotateAroundPivot(-180, new Vector2(Screen.width*0.5f,Screen.height*0.5f));
		GUIUtility.RotateAroundPivot(-90, pivot);
	}
	
	
	
	public void setGuiActivity(bool isActive, int player){
		//print("GUI set to active " + isActive + " for player " + player);
		if(isActive){
			_obStyle.normal.background = _activeOptionButton;
			_obStyle.active.background = _activeOptionButton;
			if(player == 1){
				_obTouchRect = _obTouchRectP1;
			} else {
				_obTouchRect = _obTouchRectP2;
			}
		} else {
			_obStyle.normal.background = _inactiveOptionButton;
			_obStyle.active.background = _inactiveOptionButton;
			_obTouchRect = new Rect(0,0,0,0);
		}
	}
}
