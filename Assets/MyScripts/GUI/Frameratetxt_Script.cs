using UnityEngine;
using System.Collections;

public class Frameratetxt_Script : MonoBehaviour
{
	float _currentFrameRate;
	
	void Start ()
    {
		StartCoroutine(sleepTight());
	}
	
	void Update ()
    {
        guiText.text = "Framerate: " + _currentFrameRate;
	}
	
	public IEnumerator sleepTight()
    {
		for(;;)
        {
			_currentFrameRate = 1f/Time.deltaTime;
			yield return new WaitForSeconds(0.5f);
		}
	}
}
	