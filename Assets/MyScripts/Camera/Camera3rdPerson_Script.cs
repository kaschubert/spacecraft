using UnityEngine;
using System.Collections;

public class Camera3rdPerson_Script : MonoBehaviour
{

    //Camera
    private Vector3     _oldCamPos;
    private Vector3     _newCamPos;
    public Vector3     _currentCamPos;

    private Quaternion  _oldCamOrient;
    private Quaternion  _newCamOrient;
    public Quaternion  _currentCamOrient;

    public float        _camHeight;
    public float        _camZOffset;
    public float        _camMoveDrag;
    public float        _camRotateDrag;
    
    public Transform    _lookAt;

	void Start () 
    {
	    //Initialize Camera
        _currentCamPos      = _lookAt.position;
        _currentCamOrient   = _lookAt.rotation;
        
        _currentCamPos.y += _camHeight;
        _currentCamPos.z += _camZOffset;

        Quaternion rotateX = Quaternion.AngleAxis(90, _lookAt.right);
        _currentCamOrient *= rotateX;
        
        transform.parent.position = _currentCamPos;
        transform.rotation = _currentCamOrient;
	}
	
	void Update ()
    {
        _oldCamPos = _currentCamPos;
        _oldCamOrient = _currentCamOrient;

        _newCamPos = _lookAt.position;
        //_newCamPos.y = _camHeight;
        //_newCamPos.z += _camZOffset;

        _newCamOrient = transform.rotation;

        _currentCamPos = Vector3.Lerp(_oldCamPos, _newCamPos, Time.smoothDeltaTime/_camMoveDrag);
        _currentCamOrient = Quaternion.Slerp(_oldCamOrient, _newCamOrient, Time.smoothDeltaTime/_camRotateDrag);

        transform.rotation = _currentCamOrient;
        transform.parent.transform.position = _currentCamPos;
	}
}
