using UnityEngine;
using System.Collections;

public class Shot_Script : MonoBehaviour
{

    public float                    _totalTime = 8.0f;
	public TurretControl_Script		_origin;
	protected float                 _timeFlying = 0.0f;
    public bool                     _active;
    public GameObject               _target;
	public GameObject				_parent;
//    private Collider                _targetCollider;
//    private Attributes_Script       _targetAttributes;
	protected string				_shipname;
	protected GameObject			_model;
	
	public int    					_damage2Hunter;
	public int    					_damage2Cruiser;
	public int    					_damage2Gunship;
    
	void Start () {
		Start4Derived();
	}
	
	protected virtual void Start4Derived()
	{
	}
	
	protected virtual void Update4Derived()
	{
	}
	
	void Update () {		
        if (_active)
        {
            _timeFlying += Time.deltaTime;

            if (_timeFlying > _totalTime)
            {
				if(gameObject)
					_origin.decrementShotCount();
				print("destroyed cause of time");
                Destroy(gameObject);
            }
			
			Update4Derived();
        }
	}

//	void OnCollisionEnter(Collision obstacle){
//		
//		print("COLLIDING");
//		
//		if (_active && obstacle.gameObject.tag == "Ship")
//        {
//			string name = obstacle.gameObject.name;
//			
//			if(name.Equals("Crusier_P2(Clone)") || name.Equals("Crusier_P1(Clone)"))
//			{
//				_targetAttributes._currentHealth -= _damage2Cruiser;
//			}
//			else if(name.Equals("Gunship_lowPoly(Clone)"))
//			{
//				_targetAttributes._currentHealth -= _damage2Gunship;
//			}
//			else if(name.Equals("Hunter_Squad(Clone)"))
//			{
//				_targetAttributes._currentHealth -= _damage2Hunter;
//			}
//			
//			explode();
//        }
//	}

    void OnTriggerEnter(Collider other)
    {
		if(_parent){
	        if (_active && other.gameObject.tag == "Ship" && (other.gameObject != _parent.transform.root.gameObject))
	//        if (_active && other.collider == _targetCollider)
			{
				_target = other.gameObject;
				string name = other.gameObject.name;
				
				if(name.Equals("Crusier_P2(Clone)") || name.Equals("Crusier_P1(Clone)"))
				{
					other.gameObject.GetComponent<Attributes_Script>()._currentHealth -= _damage2Cruiser;
	//				_targetAttributes._currentHealth -= _damage2Cruiser;
				}
				else if(name.Equals("Gunship_lowPoly(Clone)"))
				{
					other.gameObject.GetComponent<Attributes_Script>()._currentHealth -= _damage2Gunship;
	//				_targetAttributes._currentHealth -= _damage2Gunship;
				}
				else if(name.Equals("Hunter_Squad(Clone)"))
				{
					other.gameObject.GetComponent<Attributes_Script>()._currentHealth -= _damage2Hunter;
	//				_targetAttributes._currentHealth -= _damage2Hunter;
				}
				
				explode();
	        }
		}
    }
	
	protected virtual void explode()
	{
	}
	
	public virtual void shoot()
	{
	}

    public void setTarget(GameObject target)
    {
        _active = true;
        _target = target;
//        _targetCollider = target.collider;
//        _targetAttributes = target.GetComponent<Attributes_Script>();
    }
}
