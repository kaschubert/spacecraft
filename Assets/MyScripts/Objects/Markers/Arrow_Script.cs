using UnityEngine;
using System.Collections;

public class Arrow_Script: MonoBehaviour
{
    private Vector3[]   _newVertices;
    private Vector2[]   _newUV;
    private int[]       _newTriangles;
    public float        _length = 1;
    public float        _width = 1;
    public Vector3      _startPoint;

	void Start ()
    {
        _newVertices = new Vector3[3];
        _newTriangles = new int[3];
        
        Mesh mesh = new Mesh();

        mesh.vertices = _newVertices;
        //mesh.uv = newUV;
        mesh.triangles = _newTriangles;
        GetComponent<MeshFilter>().mesh = mesh;

        mesh.RecalculateNormals();
        renderer.enabled = false;
	}
	
	void Update ()
    {
	
	}

    public void show()
    {
        renderer.enabled = true;
    }

    public void hide()
    {
        renderer.enabled = false;
    }
    
    public void setStartPoint(Vector3 startPoint)
    {
        _startPoint = startPoint;
    }
}
