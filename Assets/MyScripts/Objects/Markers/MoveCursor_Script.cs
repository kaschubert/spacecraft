using UnityEngine;
using System.Collections;

public class MoveCursor_Script : MonoBehaviour
{
    private Vector3     _moveSource;
    private Vector3     _moveTarget;
    private bool        _drawEnabled = false;

	void Start () 
    {

	}
	
	void OnPostRender () 
    {
        Color currentColor;
        currentColor.r = 1.0f;
        currentColor.g = 1.0f;
        currentColor.b = 1.0f;
        currentColor.a = 1.0f;

        if (_drawEnabled)
        {
            GL.Color(currentColor);
            GL.Begin(GL.LINES);
                GL.Vertex3(
                    _moveSource.x,
                    _moveSource.y,
                    _moveSource.z
                );
                GL.Vertex3(
                    _moveTarget.x,
                    _moveTarget.y,
                    _moveTarget.z
                );
            GL.End();
        }
	}

    public void showLine(Vector3 moveSource, Vector3 moveTarget)
    {
        _moveSource = moveSource;
        _moveTarget = moveTarget;
        _drawEnabled = true;
    }

    public void updateTarget(Vector3 moveTarget)
    {
        _moveTarget = moveTarget;
    }

    public void hideLine()
    {
       

        _drawEnabled = false;
    }
}
