using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttackMarker_Script : MonoBehaviour {

    public List<GameObject>     _collidingATM;
    private GameObject          _parent;
    private int                 _maxTargetCount;

	void Start ()
    {
        _parent = transform.parent.gameObject;
        ShipControl_Script parentControl = _parent.GetComponent<ShipControl_Script>();
        _maxTargetCount = parentControl._atrScript._maxTargetCount;
	}
	
	void LateUpdate ()
    {
	}

    void OnTriggerEnter(Collider other)
    {
        GameObject otherShip = other.gameObject;
		print("TriggerEnter");

        //Is it a Ship and is it not the ship this script is attached to?
        if (otherShip.CompareTag("Ship") && !otherShip.Equals(_parent) && _collidingATM.Count < _maxTargetCount)
        {
            _collidingATM.Add(otherShip);
        }
    }

    void OnTriggerExit(Collider other)
    {
        GameObject otherShip = other.gameObject;
		print("TriggerExit");

        if (otherShip.CompareTag("Ship"))
        {
            _collidingATM.Remove(otherShip);
        }
    }
}
