using UnityEngine;
using System.Collections;
using System;

public class Ellipse_Script : MonoBehaviour
{
    public Vector3[]    _newVertices;
    private Vector2[]   _newUV;
    private int[]       _newTriangles;
    public float        _length = 1;
    public float        _width = 1;
    public bool         _selected;
    private double      _PiDivide180 = Math.PI / 180;

    void Start()
    {
       recalculateEllipse(_length, _width);
    }
	
	void Update ()
    {

	}

    public void show()
    {
        _selected = true;
        renderer.enabled = true;
    }

    public void hide()
    {
        _selected = false;
        renderer.enabled = false;
    }

    public void recalculateEllipse(float length, float width)
    {
        _length = length;
        _width = width;
		
		_newUV = new Vector2[61];
        _newVertices = new Vector3[61];
        _newTriangles = new int[180];
        Vector3 middlePoint = new Vector3(0.0f, 0.0f, 0.0f);
        Vector3 circlePoint = new Vector3();
        _newVertices[60] = middlePoint;

        for (int i = 0; i < 60; i++)
        {
			//Omitting these will cause constant warnings, so they're set to an arbitrary value
			_newUV[i] = new Vector2(1,1); 
			
            circlePoint.x = _width * (float)(Math.Sin(i * (6 * _PiDivide180)));
            circlePoint.y = 0.0f;
            circlePoint.z = _length * (float)(Math.Cos(i * (6 * _PiDivide180)));

            _newVertices[i] = circlePoint;

            _newTriangles[i * 3] = 60;
            _newTriangles[i * 3 + 1] = i;
            if (i < 59)
                _newTriangles[i * 3 + 2] = i + 1;
            else
                _newTriangles[i * 3 + 2] = 0;
        }		
		
		Mesh mesh = GetComponent<MeshFilter>().mesh;
		mesh.Clear();
		mesh.vertices = _newVertices;
		mesh.uv = _newUV;
        mesh.triangles = _newTriangles;
        mesh.RecalculateNormals();
		
        renderer.enabled = false;
    }
}
