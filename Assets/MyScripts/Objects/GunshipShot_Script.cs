using UnityEngine;
using System.Collections;

public class GunshipShot_Script : Shot_Script {
	
	protected override void Start4Derived()
	{
		_model = transform.FindChild("Gunship_Shot_model").gameObject; 
	}
		
	protected override void explode()
	{
		_origin.decrementShotCount();
		Destroy(gameObject);
	}
	
	public override void shoot()
	{
		rigidbody.AddRelativeForce(Vector3.forward * 7000);
	}
}
