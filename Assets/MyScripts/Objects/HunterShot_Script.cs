using UnityEngine;
using System.Collections;

public class HunterShot_Script : Shot_Script {
	
	private GameObject _missileBlast;
	
	protected override void Start4Derived()
	{
		_missileBlast = transform.FindChild("Engine_Blast").gameObject;
		_model = transform.FindChild("Missile_Shot_model").gameObject; 
	}
	
	protected override void explode()
	{
		if(_missileBlast)
			_missileBlast.particleEmitter.emit = false;
		_origin.decrementShotCount();
		Destroy(gameObject);
	}
	
	public override void shoot()
	{
		rigidbody.AddRelativeForce(Vector3.forward * 5000);
	}
}
