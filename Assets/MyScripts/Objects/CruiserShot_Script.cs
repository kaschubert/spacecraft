using UnityEngine;
using System.Collections;

public class CruiserShot_Script : Shot_Script {
	
	public float _lengthOfLaser = 4.0f;
	private float _wideScale = 25f;
	private float _smallScale = 3f;
	private float _speed;
	
	protected override void Start4Derived()
	{
		_model = transform.FindChild("Cruiser_Shot_model").gameObject; 
		_speed = 3;
	}
	
	protected override void Update4Derived()
	{
		Vector3 scale;
		float xScale;
		
//		print(_timeFlying);
			
		if(_timeFlying < (_lengthOfLaser/2))
		{
			xScale = Mathf.Lerp(transform.localScale.x, _wideScale, Time.deltaTime * _speed);
			scale = transform.localScale;
			scale.x = xScale;
//			print("upscale.x" + xScale);
			transform.localScale = scale;
		}
		else if(_timeFlying > (_lengthOfLaser/2) && _timeFlying < _lengthOfLaser)
		{
			xScale = Mathf.Lerp(transform.localScale.x, _smallScale, Time.deltaTime * _speed);
			scale = transform.localScale;
			scale.x = xScale;
//			print("downscale.x" + xScale);
			transform.localScale = scale;
		}
		else if (_timeFlying > _lengthOfLaser)
        {
//			print("destroyed in cruisershot");
            Destroy(gameObject);
        }
	}
	
	protected override void explode()
	{
		_origin.decrementShotCount();
//		print("destroyed in explode");
//		Destroy(_model);
	}
	
	public override void shoot()
	{
		Vector3 parentPos = _parent.transform.position;
		Vector3 targetPos = _target.transform.position;
		float distance = Vector3.Distance(parentPos, targetPos);

		//Scale to reach target
		Vector3 dist = new Vector3(0, 0, distance);
		transform.localScale += dist * 2;
		
		Vector3 targetDirection = targetPos - parentPos;
		
		transform.rotation =  Quaternion.LookRotation(targetDirection);
	}
}
