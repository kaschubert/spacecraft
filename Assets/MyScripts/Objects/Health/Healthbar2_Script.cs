using UnityEngine;
using System.Collections;

public class Healthbar2_Script : MonoBehaviour {

	private GameObject _currentHealthBar;
	private GameObject _baseHealthBar;
	private GameObject _bgHealthBar;
	private Bounds _bounds; 
	
	private Attributes_Script _attr_Script;
	private float _maxHealth;
	private float _currentHealth;
	private MeshFilter _mf;
	
	// Use this for initialization
	void Start () {
		
		//Bounds are needed to calculate the position of the healthbar
		//Hunter-Squads are almost square, so they don't need a MeshFilter to recalculate their bounds
		if(transform.root.name.Contains("Hunter")){
			_bounds = transform.root.GetComponent<Collider>().bounds;
		} else if(transform.root.name.Contains("Gunship")) {
			_mf = transform.root.FindChild("Gunship_lowPoly_model").GetComponent<MeshFilter>();
			_bounds = _mf.renderer.bounds;
		} else {
			_mf = transform.root.FindChild("Cruiser_lowPoly_model").GetComponent<MeshFilter>();
			_bounds = _mf.renderer.bounds;
		}
		_currentHealthBar = transform.FindChild("current").gameObject;
		_baseHealthBar = transform.FindChild("base").gameObject;
		_bgHealthBar = transform.FindChild("background").gameObject;
		
		_attr_Script = transform.root.GetComponent<Attributes_Script>();
		_maxHealth = _attr_Script._maxHealth;
		_currentHealth = _attr_Script._currentHealth;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(_mf){
			_mf.mesh.RecalculateBounds();
			_bounds = _mf.renderer.bounds;
		}
		
		
		_currentHealth = _attr_Script._currentHealth;
		
		Vector3 lowerLeftCorner = transform.root.transform.position - _bounds.extents;
		
		lowerLeftCorner.y = 15f;
		_bgHealthBar.transform.position = lowerLeftCorner;
		
		lowerLeftCorner.y = 15.1f;
		_baseHealthBar.transform.position = lowerLeftCorner;
		
		lowerLeftCorner.y = 15.2f;
		_currentHealthBar.transform.position = lowerLeftCorner;
		
		float totalTranslateX = _bgHealthBar.transform.localScale.x * 0.5f + transform.position.x;
//		float totalTranslateX = 1000f;
//		float totalTranslateZ = _bgHealthBar.transform.localScale.z * 0.5f + transform.position.z;
//		float totalTranslateZ = 1000f;
		Vector3 totalTransPos = new Vector3(totalTranslateX, transform.position.y, transform.position.z);
		transform.position = totalTransPos;
		
		//ScaleVector with 30/3/1 is full health
		//Determine current health factor
		float healtFactor = _currentHealth/_maxHealth;
		float lengthOfHealth = _baseHealthBar.transform.localScale.x * healtFactor;
		
		//Scale the currentHealthbar to a length representing the health
		//and adjust the position of the bar to be aligned left
		_currentHealthBar.transform.localScale = new Vector3(lengthOfHealth, _baseHealthBar.transform.localScale.y, 1f);
		float curTranslateX = _currentHealthBar.transform.position.x -((_baseHealthBar.transform.localScale.x/2f)-(lengthOfHealth/2f));
		Vector3 curTransXPos = new Vector3(curTranslateX, _currentHealthBar.transform.position.y, _currentHealthBar.transform.position.z);
		_currentHealthBar.transform.position = curTransXPos;
	}
	
//	public void setAlphas(float alpha){
//		Material mat = _currentHealthBar.GetComponent<Material>();
//	}

}
