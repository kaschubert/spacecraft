using UnityEngine;
using System.Collections;

public class Spawner_Script : MonoBehaviour {

	public GameObject 			_2Spawn;
	public int 					_numberOfSpawns;	
	
	void Start () 
	{
	}
	
	public void spawn(Vector2 xRange, Vector2 yRange) 
	{
		//Spawn anything in space x {-300, 300} and y {-200, 200}
		
		for(int i = 0 ; i < _numberOfSpawns; i++)
		{
			Vector3 position = new Vector3(Random.Range(xRange.x, xRange.y), 0, Random.Range(yRange.x, yRange.y));
//			GameObject spawn = (GameObject) Instantiate(_2Spawn, position, gameObject.transform.rotation);
			Instantiate(_2Spawn, position, gameObject.transform.rotation);
		}
	}
	
}
