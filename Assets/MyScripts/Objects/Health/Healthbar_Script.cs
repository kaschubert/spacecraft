using UnityEngine;
using System.Collections;

public class Healthbar_Script : MonoBehaviour 
{
    public float                _oldCurHealth;
//	private RaycastHit          _hit;
	private GameObject          _billboard;
	private Attributes_Script   _atrScript;
    private Texture2D           _tex;
    private Color               _deathColor;
    private Color               _lifeColor;
	public float                _healthBarLength;
	
	
	// Use this for initialization
	void Start ()
    {
        _atrScript = GetComponent<Attributes_Script>();
		//healthBarLength = maxBarSize;
//		_hit = new RaycastHit();
		//_billboard = transform.Find("Billboard");
		_billboard = transform.FindChild("Healthbar").gameObject;
		
//		Vector3 lowerLeftBoundingBoxCorner = gameObject.renderer.bounds.max;
		Vector3 test = new Vector3(1000,1000,1900);
		
		_billboard.transform.position = test;

        _tex = new Texture2D(64, 64, TextureFormat.ARGB32, false);

        _deathColor = _billboard.renderer.material.GetColor("_deathColor");
        _lifeColor = _billboard.renderer.material.GetColor("_lifeColor");

        float border = (_tex.width / _atrScript._maxHealth) * _atrScript._currentHealth;

        //print("border: " + border);

        for(int y = 0; y < _tex.height; y++)
        {
            for (int x = 0; x < _tex.width; x++)
            {
                if(x > border)
                    _tex.SetPixel(x, y, _deathColor);
                else
                    _tex.SetPixel(x, y, _lifeColor);
            }
        }

        _tex.Apply();
        _billboard.renderer.material.SetTexture("_barTex", _tex);
	}
	
	
	// Update is called once per frame
    // Optimization: maybe use SetPixels with an Array as big as the texture, 
    // but only optimizing for large textures
	void Update ()
    {
        float curHealth = _atrScript._currentHealth;

        if (_oldCurHealth != curHealth)
        {
            float border = (_tex.width / _atrScript._maxHealth) * curHealth;

            for (int y = 0; y < _tex.height; y++)
            {
                for (int x = 0; x < _tex.width; x++)
                {
                    if (x >= border)
                        _tex.SetPixel(x, y, _deathColor);
                    else
                        _tex.SetPixel(x, y, _lifeColor);
                }
            }
            _tex.Apply();
        }

        _oldCurHealth = curHealth;
	}
}