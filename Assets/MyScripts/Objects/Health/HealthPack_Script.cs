using UnityEngine;
using System.Collections;

public class HealthPack_Script : MonoBehaviour
{
    public int                  _healingAmount;
//    private Random              _random;
    private Quaternion          _targetRotation;
    private GameObject          _parent;
    private Explosion_Script    _explosionScript;
	
	void Start ()
    {
//        _random = new Random();
        _targetRotation = Random.rotation;
        _parent = transform.parent.gameObject;
        
        _explosionScript = _parent.GetComponent<Explosion_Script>();
	}
	
	
	void Update ()
    {
        if (Quaternion.Angle(transform.rotation, _targetRotation) < 1)
        {
            _targetRotation = Random.rotation;
        }

        transform.rotation = Quaternion.Lerp(transform.rotation, _targetRotation, Time.deltaTime);
	}
	
	//Healthpack is collected
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Ship")
        {
            Attributes_Script _atrScript = other.gameObject.GetComponent<Attributes_Script>();
            _atrScript._currentHealth += _healingAmount;
            if (_atrScript._currentHealth > _atrScript._maxHealth)
            {
                _atrScript._currentHealth = _atrScript._maxHealth;
            }
            _parent.audio.Play();
            _explosionScript.init();
            Destroy(gameObject);
        }
    }
}
