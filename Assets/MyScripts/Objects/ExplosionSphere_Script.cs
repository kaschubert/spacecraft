using UnityEngine;
using System.Collections;

public class ExplosionSphere_Script : MonoBehaviour {
	
	public float _radius = 0.0f;
	public float _damage = 0.0f;
	private float _deltaTime = 0.0f;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		float x = Mathf.Lerp(0.0f, _radius, 10.0f*_deltaTime);
		float y = Mathf.Lerp(0.0f, _radius, 10.0f*_deltaTime);
		float z = Mathf.Lerp(0.0f, _radius, 10.0f*_deltaTime);
		
		_deltaTime += Time.deltaTime;
		
		transform.localScale = new Vector3(x, y, z);
		
		if(x >= _radius){
			Destroy(gameObject);
		}
	}
	
	void OnTriggerEnter(Collider victim){
		if(victim.gameObject){
			if (victim.gameObject.tag == "Ship")
			{		
				float distanceModifier = 0.0f;
				if(transform.localScale.x != 0){
					distanceModifier = 1.0f - (transform.localScale.x/_radius);
				}
				
				Attributes_Script attrs = victim.gameObject.GetComponent<Attributes_Script>();
				attrs._currentHealth -= _damage*(distanceModifier);
	        }
		}
	}
	
	
}
