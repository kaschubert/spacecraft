using UnityEngine;
using System.Collections;

public static class Fader_Script {
	
	private static float fadeInDeltaTime = 0f;
	private static float fadeOutDeltaTime = 0f;
	private static float alphaFadeIn = 0f;
	private static float alphaFadeOut = 1f;
	
	
	//Returns an Alpha-value that approaches 1 
	public static float fadeIn(float startingAlpha, float endingAlpha){
				
		if(alphaFadeIn < endingAlpha){
			fadeInDeltaTime += Time.deltaTime;
			alphaFadeIn = Mathf.Lerp(startingAlpha, endingAlpha, fadeInDeltaTime);
		} else {
			fadeInDeltaTime = 0f;
			alphaFadeIn = 0f;
		}
		
		return alphaFadeIn;
	}
	
	//Returns an Alpha-value that approaches 1 
	public static float fadeOut(float startingAlpha, float endingAlpha){
				
		if(alphaFadeOut > endingAlpha){
			fadeOutDeltaTime += Time.deltaTime;
			alphaFadeOut = Mathf.Lerp(startingAlpha, endingAlpha, fadeOutDeltaTime);
		} else {
			fadeOutDeltaTime = 0f;
			alphaFadeOut = 1f;
		}
		
		return alphaFadeOut;
	}
	
}
