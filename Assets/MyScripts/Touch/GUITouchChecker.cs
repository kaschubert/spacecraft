using UnityEngine;
using System.Collections;

public class GUITouchChecker : MonoBehaviour {
	
	private BBTouchEventManager _eventManager;
	private GUI_Script _guiScript;
//	private bool _guiTouched = false;
	private float timeGone = 0;
	
	// Use this for initialization
	void Start () {
		_eventManager = BBTouchEventManager.instance;
		_guiScript = GameObject.Find("GUI").GetComponent<GUI_Script>();
	}
	
	void OnApplicationQuit() {
        _eventManager = null;
    }
	
	// Update is called once per frame
	void Update () {
		timeGone += Time.deltaTime;
		foreach ( BBTouchEvent anEvent in _eventManager.activeEvents.Values) {
			
			if (anEvent.eventState == BBTouchEventState.Began) {
				
				if(timeGone > 0.3){
//					_guiTouched = _guiScript.touchCheckGUI(anEvent.screenPosition);
					_guiScript.touchCheckGUI(anEvent.screenPosition);
					timeGone = 0.0f;
				}
//				print("Gui has been touched!: " + anEvent.screenPosition);
			}
		}
	}
}
